package com.alshaya.pojo

class Profile {
    var data = ProfileData()
}

class ProfileData {
    var id = ""
    var name = ""
    var email = ""
    var image: String? = null
    var login_id = ""
    var mobile_country_code = ""
    var mobile_no = ""
    var country_name = ""
    var email_operation_team = ""
    var call_operation_team = ""
    var chat_on_whatsapp = ""
    var sites = ArrayList<Sites>()
}

class Sites {
    var id = ""
    var email = ""
    var name = ""
}