package com.alshaya.pojo

import java.io.Serializable

class ScanPalletIdPojo:Serializable {

    var data = DataBean()
    var message = ""

    class DataBean:Serializable {
        var id = 0
        var pallet_id: String = ""
        var total_box = 0
        var scannedBox = 0
        var courier_id = ""
        var sign_as_dispatcher_status = ""
        var sign_as_dispatcher_date = ""
        var sign_as_receiver_status = ""
        var sign_as_receiver_date = ""
        var sign_as_security_status = ""
        var sign_as_security_date = ""
        var sts_receiving = ""
        var sts_receiving_date = ""

        var box_data = ArrayList<BoxDataBean>()
    }

    class BoxDataBean:Serializable{
        var id = 0
        var awb_bill_number: String = ""
        var weight: String = ""
        var sts_receiving_status: String = ""
        var dispatch_status: String = ""
        var isScanned = 0
        var isFirst = 0

    }
}

class DispatchBean:Serializable{
    var id = 0
    var awb_bill_number: String = ""
    var weight: String = ""
    var sts_receiving_status: String = ""
    var isScanned = 0
    var isFirst = 0

}