package com.alshaya.pojo

import java.io.Serializable

class TrackPalletMDRPojo : Serializable {

    var total_box = 0
    var type: String = ""
    var pallet_details = PalletDetailsBean()


    class PalletDetailsBean : Serializable {
        var id = 0
        var admin_id = 0
        var pallet_id: String = ""
        var message: String = ""
        var type: String = ""
        var is_lock_pallet_id = 0
        var dispatch_user_id: String = ""
        var dispatch_status: String = ""
        var dispatch_date: String = ""
        var courier_id: String = ""
        var courier_date: String = ""
        var store_user_id: String = ""
        var store_id: String = ""
        var store_date: String = ""
        var sign_as_dispatcher_status: String = ""
        var sign_as_dispatcher_date: String = ""
        var sign_as_receiver_status: String = ""
        var sign_as_receiver_date: String = ""
        var sign_as_security_status: String = ""
        var sign_as_security_date: String = ""
        var sts_receiving_user_id: String = ""
        var sts_receiving_status: String = ""
        var sts_receiving_date: String = ""
        var sts_dispatch_status: String = ""
        var sts_dispatch_user_id = 0
        var sts_dispatch_date: String = ""
        var created_at: String = ""
        var updated_at: String = ""
    }
}

class PendingRequest {
    var data = ArrayList<TrackPalletMDRPojo.PalletDetailsBean>()
}