package com.alshaya.utils

import android.accounts.NetworkErrorException
import android.content.Context
import android.net.ParseException
import android.util.Log
import android.widget.Toast
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException


/**
Created by Arth on 13-Jul-17.
 */

class RetrofitHelper {

    private var gsonAPI: API
    private var connectionCallBack: ConnectionCallBack? = null

    constructor() {

        val httpClient = UnsafeOkHttpClient.unsafeOkHttpClient.newBuilder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                /*.header(
                    "Accept-Language",
                    if (StoreUserData(context).getString(Constants.USER_LANGUAGE).isEmpty()) {
                        "en"
                    } else StoreUserData(context).getString(Constants.USER_LANGUAGE)
                )
                .header(
                    "Authorization",
                    StoreUserData(context).getString(Constants.USER_TOKEN)
                )*/
                .method(original.method(), original.body())
                .build()

            chain.proceed(request)
        }

        //val okHttpClient = httpClient.build()
        val TIMEOUT = 2 * 60 * 1000
        val gsonretrofit = Retrofit.Builder()
            .baseUrl(Constants.URL)
            .client(
                httpClient.connectTimeout(
                    TIMEOUT.toLong(),
                    TimeUnit.SECONDS
                ).readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).writeTimeout(
                    TIMEOUT.toLong(),
                    TimeUnit.SECONDS
                ).build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        gsonAPI = gsonretrofit.create(API::class.java)


    }

    fun api(): API {
        return gsonAPI
    }


    fun callApi(
        activity: Context,
        call: Call<ResponseBody>,
        callBack: ConnectionCallBack,
    ) {
        if (!Utils.isOnline(activity)) {
            Utils.internetAlert(activity)
            return
        }
        connectionCallBack = callBack
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.code() == 200) {
                    if (connectionCallBack != null)
                        connectionCallBack!!.onSuccess(response)
                } else if (response.code() == 401) {
                    StoreUserData(activity).clearData()
                } else if (response.code() == 500) {
                    Toast.makeText(
                        activity,
                        "Internal Server Error!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    try {
                        val res = response.errorBody()!!.string()
                        Log.i("TAG", "onResponse: $res")
                        if (connectionCallBack != null)
                            connectionCallBack!!.onError(response.code(), res)
                    } catch (e: IOException) {
                        Log.i("TAG", "onResponse: " + call.request().url())
                        e.printStackTrace()
                        if (connectionCallBack != null)
                            connectionCallBack!!.onError(response.code(), e.message)
                    } catch (e: NullPointerException) {
                        Log.i("TAG", "onResponse: " + call.request().url())
                        e.printStackTrace()
                        if (connectionCallBack != null)
                            connectionCallBack!!.onError(response.code(), e.message)
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, error: Throwable) {
                var message: String? = null
                when (error) {
                    is NetworkErrorException -> {
                        message = "Please check your internet connection"
                    }
                    is ParseException -> {
                        message = "Parsing error! Please try again after some time!!"
                    }
                    is TimeoutException -> {
                        message = "Connection TimeOut! Please check your internet connection."
                    }
                    is UnknownHostException -> {
                        message = "Please check your internet connection and try later"
                    }
                    is Exception -> {
                        message = error.message
                    }
                }
                if (connectionCallBack != null)
                    connectionCallBack!!.onError(-1, message)
            }
        })
    }

    interface ConnectionCallBack {
        fun onSuccess(body: Response<ResponseBody>)

        fun onError(code: Int, error: String?)
    }

    companion object {

        fun createPartFromString(descriptionString: String): RequestBody {
            return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString
            )
        }

        fun prepareFilePart(partName: String, filePath: String): MultipartBody.Part {
            val requestFile = RequestBody.create(
                MediaType.parse("image/*"),
                File(filePath)
            )
            return MultipartBody.Part.createFormData(partName, File(filePath).name, requestFile)
        }
    }
}

/******************USAGE**********************
 *
private fun getSectionList() {
showProgress()
val retrofitHelper = RetrofitHelper()
var call: Call<ResponseBody> =
retrofitHelper.api().get_section_by_class(
storeUserData.getString(Constants.USER_LANGUAGE_ID),
storeUserData.getString(Constants.USER_SCHOOL_ID),
intent.getStringExtra("classId")
)
retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
override fun onSuccess(body: Response<ResponseBody>) {
dismissProgress()
if (body.code() != 200) {
return
}
val responseString = body.body()!!.string()
Log.i("TAG", responseString)
if (JSONObject(responseString).getString("code") == "LOGOUT") {
startActivity(
Intent(activity, LoginActivity::class.java)
.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
)
return
}
var dashboard = Gson().fromJson(responseString, Dashboard::class.java)
if (dashboard.success == 1 && !dashboard.data.all_section_list.isNullOrEmpty()) {
var pojo = ClassPojo()
pojo.section_id = "0"
pojo.class_id = "0"
pojo.name = "ALL"
dashboard.data.all_section_list.add(0, pojo)
searchList.addAll(dashboard.data.all_section_list)
allClass.addAll(dashboard.data.all_section_list)
adapter.notifyDataSetChanged()
} else {
showAlert(dashboard.message)
}
}

override fun onError(code: Int, error: String?) {
dismissProgress()
Log.i("error", error)
}
})
}
 */