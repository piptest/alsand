package com.alshaya.utils

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.alshaya.controls.CTextView
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import okhttp3.OkHttpClient
import java.io.InputStream
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object Utils {
    @GlideModule
    class UnsafeOkHttpGlideModule : AppGlideModule() {

        override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
            registry.replace(
                GlideUrl::class.java, InputStream::class.java,
                OkHttpUrlLoader.Factory(UnsafeOkHttpClient.unsafeOkHttpClient)
            )
        }
    }

    fun getAgo(seconds: Long): String {
        val past = Date(seconds * 1000)
        val now = Date()
        val seconds: Long = TimeUnit.MILLISECONDS.toSeconds(now.time - past.time)
        val minutes: Long = TimeUnit.MILLISECONDS.toMinutes(now.time - past.time)
        val hours: Long = TimeUnit.MILLISECONDS.toHours(now.time - past.time)
        val days: Long = TimeUnit.MILLISECONDS.toDays(now.time - past.time)
        if (seconds < 60) {
            return ("${seconds}s")
        } else if (minutes < 60) {
            return ("${minutes}m")
        } else if (hours < 24) {
            return ("${hours}h")
        } else if (days >= 7) {
            if (days > 360) {
                return "${(days / 360)}Y"
            } else if (days > 30) {
                return "${(days / 30)}M"
            } else {
                return "${(days / 7)}W"
            }
        } else /*if (days < 7)*/ {
            return ("${days}d")
        }
    }


    fun getDeviceName(): String {
        return Build.MANUFACTURER + "_" + Build.MODEL + "_" + Settings.Secure.ANDROID_ID
    }

    fun internetAlert(activity: Context) {
        AlertDialog.Builder(activity)
            .setMessage("Please check internet connection.")
            .setPositiveButton("OK") { dialogInterface, i ->
                dialogInterface.dismiss()
            }
            .create()
            .show()
    }

    fun isOnline(context: Context): Boolean {
        val connectivity = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connectivity.allNetworkInfo
        for (i in info.indices)
            if (info[i].state == NetworkInfo.State.CONNECTED) {
                return true
            }
        return false
    }

    fun isValidEmail(email: EditText): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()
    }

    fun selectDate(activity: Activity, btnDate: CTextView) {
        DatePickerDialog(
            activity,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                btnDate.text =
                    (monthOfYear + 1).toString() + "/" + dayOfMonth.toString() + "/" + year
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DATE, dayOfMonth)
                //calendar.getTimeInMillis() + "";


            },
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        ).show()

    }


    fun selectTime(activity: Activity, btnTime: CTextView) {
        TimePickerDialog(
            activity,
            TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                btnTime.text = "$selectedHour:$selectedMinute"
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour)
                calendar.set(Calendar.MINUTE, selectedMinute)
            },
            Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
            Calendar.getInstance().get(Calendar.MINUTE),
            false
        ).show() //is24HourView
    }

    fun isEmpty(view: View): Boolean {
        if (view is EditText) {
            if (view.text.toString().isEmpty()) {
                return true
            }
        } else if (view is Button) {
            if (view.text.toString().isEmpty()) {
                return true
            }
        } else if (view is TextView) {
            if (view.text.toString().isEmpty()) {
                return true
            }
        }
        return false
    }

    fun showListDialog(activity: Activity, btnShow: CTextView, list: ArrayList<String>) {

        val builder = AlertDialog.Builder(activity)
        val dataAdapter = ArrayAdapter(
            activity,
            android.R.layout.simple_dropdown_item_1line, list
        )
        //pass custom layout with single textview to customize list
        builder.setAdapter(dataAdapter) { dialog, which -> btnShow.text = list[which] }
        val dialog = builder.create()
        dialog.show()
    }

    fun hideKB(activity: Activity, view: View?) {
        //View view = this.getCurrentFocus();
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun setTheme(activity: Activity) {
        if (StoreUserData(activity).getString(Constants.USER_LANGUAGE).isEmpty()) {
            StoreUserData(activity).setString(Constants.USER_LANGUAGE, "ar")
        }
        val locale = Locale(StoreUserData(activity).getString(Constants.USER_LANGUAGE))
        Locale.setDefault(locale)
        val config = Configuration()

        config.setLocale(locale)
        activity.baseContext.resources.updateConfiguration(
            config,
            activity.baseContext.resources.displayMetrics
        )

    }

    object UnsafeOkHttpClient {
        // Create a trust manager that does not validate certificate chains
        val unsafeOkHttpClient:

        // Install the all-trusting trust manager

        // Create an ssl socket factory with our all-trusting manager
                OkHttpClient
            get() = try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(
                    object : X509TrustManager {
                        @Throws(CertificateException::class)
                        override fun checkClientTrusted(
                            chain: Array<X509Certificate>,
                            authType: String,
                        ) {
                        }

                        @Throws(CertificateException::class)
                        override fun checkServerTrusted(
                            chain: Array<X509Certificate>,
                            authType: String,
                        ) {
                        }

                        override fun getAcceptedIssuers(): Array<X509Certificate> {
                            return arrayOf()
                        }
                    }
                )

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                val builder = OkHttpClient.Builder()
                builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                builder.hostnameVerifier { hostname, session -> true }
                builder.build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
    }
}


