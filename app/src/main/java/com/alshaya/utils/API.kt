package com.alshaya.utils

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface API {
    @FormUrlEncoded
    @POST("api/v1/login")
    fun login(
        @Field("login_id") login_id: String,
        @Field("device_type") device_type: String,
        @Field("device_token") device_token: String,
    ): Call<ResponseBody>

    @GET("api/v1/get-employee-permission")
    fun home(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @GET("api/v1/get-profile")
    fun profile(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @GET("api/v1/couriers")
    fun couriers(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @GET("api/v1/stores")
    fun stores(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/qc-checking-app")
    fun qcChecking(
        @Header("Authorization") token: String,
        @Field("awb_no") awb_no: String,
        @Field("awb_id") awb_id: String,
        @Field("qty_matching") qty_matching: Int,
        @Field("proper_packing") proper_packing: Int,
        @Field("sku_matching") sku_matching: Int,
        @Field("any_damage_items") any_damage_items: Int,
        @Field("remarks") remarks: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/track-pallet-mdr-id")
    fun track_Pallet_MDR_id(
        @Header("Authorization") token: String,
        @Field("pallet_id") pallet_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/app-pending-request")
    fun getPendingRequest(
        @Header("Authorization") token: String,
        @Field("status") status: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @GET("api/v1/packer")
    fun packer(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @Multipart
    @POST("api/v1/packer")
    fun addPacker(
        @Header("Authorization") token: String,
        @Part("awb_bill_number") awb_bill_number: RequestBody,
        @Part("ils_order_no") ils_order_no: RequestBody,
        @Part("awb_ils_order_no") awb_ils_order_no: RequestBody,
        @Part("box_type") box_type: RequestBody,
        @Part("scan_awb_bill_number") scan_awb_bill_number: RequestBody,
        @Part image_1: ArrayList<MultipartBody.Part>,
        @Part("site_id") site_id: RequestBody,
    ): Call<ResponseBody>

    @GET("api/v1/sagregation")
    fun sagregation(
        @Header("Authorization") token: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/sagregation")
    fun addSagregation(
        @Header("Authorization") token: String,
        @Field("awb_bill_number") awb_bill_number: String,
        @Field("pallet_id") pallet_id: String,
        @Field("weight") weight: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/last-sagregation-delete")
    fun deleteLastSagregation(
        @Header("Authorization") token: String,
        @Field("id") id: String,
        @Field("awb_bill_number") awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>


    @DELETE("api/v1/packer/{id}")
    fun deletePacker(
        @Header("Authorization") token: String,
        @Path("id") id: String,
    ): Call<ResponseBody>


    @GET("api/v1/generate-pdf/{palletId}")
    fun getPdf(
        @Header("Authorization") token: String,
        @Path("palletId") id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/prefix-awb-number")
    fun awbNumber(
        @Header("Authorization") token: String,
        @Field("scan_awb_bill_number") scan_awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/check-ils-number")
    fun checkIlsNumber(
        @Header("Authorization") token: String,
        @Field("ils_order_no") ils_order_no: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    /* @FormUrlEncoded
     @POST("api/v1/sign-as-dispatcher")
     fun signInAsDispatcher(
         @Header("Authorization") token: String,
         @Field("dispatch_id") dispatch_id: String,
         @Field("name") name: String,
         @Field("vehicle_number") vehicle_number: String,
         @Field("vehicle_number") vehicle_number: String,
     ): Call<ResponseBody>
 */
    @FormUrlEncoded
    @POST("api/v1/track-awb-number")
    fun trackAWBNumber(
        @Header("Authorization") token: String,
        @Field("awb_bill_number") awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/v1/lock-un-pallet-id")
    fun lockPalletId(
        @Header("Authorization") token: String,
        @Field("pallet_id") pallet_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @GET("api/v1/dispatch/{id}")
    fun scanPalletId(
        @Header("Authorization") token: String,
        @Path("id") id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/dispatch")
    fun addDispatch(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/sts-receiving-pallet-id-details")
    fun stsReceivingPalletId(
        @Header("Authorization") token: String,
        @Field("pallet_id") pallet_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/v1/sts-receiving-awb-number")
    fun stsReceiveingAwbNumber(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("awb_bill_number") awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/check-awb-number")
    fun checkAWBNumber(
        @Header("Authorization") token: String,
        @Field("awb_bill_number") awb_bill_number: String,
        @Field("pallet_id") pallet_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/prefix-qc-awb-number")
    fun prefixQCAWBNumber(
        @Header("Authorization") token: String,
        @Field("scan_awb_bill_number") awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/prefix-sagregation-awb-number")
    fun prefixAwbSegregation(
        @Header("Authorization") token: String,
        @Field("scan_awb_bill_number") awb_bill_number: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/dispatch-awb-number")
    fun dispatchAwbNumber(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: Int,
        @Field("awb_numbers") awb_numbers: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST
    fun signAsDispatcher(
        @Url url: String,
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("name") name: String,
        @Field("vehicle_number") vehicle_number: String,
        @Field("sign") sign: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST
    fun STSsignAsDispatcher(
        @Url url: String,
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("name") name: String,
        @Field("vehicle_number") vehicle_number: String,
        @Field("sign") sign: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/sts-receiving")
    fun stsReceiving(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("awb_bill_number[]") awb_bill_number: ArrayList<String>,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/sts-add-dispatch")
    fun stsAddDispatch(
        @Header("Authorization") token: String,
        @Field("pallet_id") pallet_id: String,
        @Field("awb_bill_number[]") awb_bill_number: ArrayList<String>,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

  @FormUrlEncoded
    @POST("api/v1/sts-dispatch-last-delete")
    fun stsDispatchLastDelete(
        @Header("Authorization") token: String,
        @Field("awb_id") awb_id: String,
        @Field("mdr_id") mdr_id:String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/v1/check-mdr-id")
    fun checkMRDID(
        @Header("Authorization") token: String,
        @Field("pallet_id") pallet_id: String, @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/dispatch-courier")
    fun dispatchCourier(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("courier_id") courier_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/v1/sts-dispatch-store")
    fun dispatchStore(
        @Header("Authorization") token: String,
        @Field("dispatch_id") dispatch_id: String,
        @Field("store_id") store_id: String,
        @Field("site_id") site_id: String,
    ): Call<ResponseBody>
}
// @Multipart
// @POST("${URL_Prefix}api/v1/updateProfile")
// fun updateProfile(
// @Header("Authorization") token: String,
// @Part("first_name") name: RequestBody,
// @Part("last_name") last_name: RequestBody,
// @Part("email") email: RequestBody,
// @Part image: MultipartBody.Part?,
// ): Call<ResponseBody>