package com.alshaya.utils

import com.alshaya.pojo.FileTypeBean

interface OnImageClick {
    fun onImageClick(position: Int, fileTypeBean: FileTypeBean)
    fun onImageDeleteClick(position: Int, fileTypeBean: FileTypeBean, id: String)
    fun onDelete(postId: Int, position: Int)
}