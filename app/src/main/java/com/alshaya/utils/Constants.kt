package com.alshaya.utils

object Constants {


    val TAG: String? = null
    const val URL = "https://ecomtracker.alshayauat.com/"

    const val USER_LANGUAGE = "USER_LANGUAGE"
    const val USER_FCM = "USER_FCM"
    const val USER_TOKEN = "USER_TOKEN"
    const val USER_IMAGE = "USER_IMAGE"
    const val USER_NAME = "USER_NAME"
    const val LOGIN_ID = "LOGIN_ID"
    const val USER_ID = "USER_ID"
    const val SITE_ID = "SITE_ID"
}