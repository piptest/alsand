package com.alshaya.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.alshaya.R
import com.alshaya.controls.CustomDialog
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import kotlinx.android.synthetic.main.activity_qcchecking.*
import kotlinx.android.synthetic.main.custom_dialog.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class QRCheckingActivity : BaseActivity() {
    var qtyMatching = 0
    var properPacking = 0
    var skuMatching = 0
    var anyDamageItem = 0
    var awb_id = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qcchecking)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener { finish() }

        scanAwbNumber.setOnClickListener {
            etQCAWBNumber.setText("")
            etQCAWBNumber.isEnabled = true
            etQCAWBNumber.requestFocus()
        }

        etQCAWBNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!Utils.isEmpty(etQCAWBNumber)) {
                    checkAWBNumber()
                    etQCAWBNumber.isEnabled = false
                }
            }

        })

        tvQtyMatchingNo.setOnClickListener {
            tvQtyMatchingNo.setBackgroundResource(R.drawable.bg_corner_one_orange)
            tvQtyMatchingNo.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvQtyMatchingYes.setBackgroundResource(R.drawable.bg_corner_two)
            tvQtyMatchingYes.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            qtyMatching = 0
        }

        tvQtyMatchingYes.setOnClickListener {
            tvQtyMatchingYes.setBackgroundResource(R.drawable.bg_corner_two_green)
            tvQtyMatchingYes.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvQtyMatchingNo.setBackgroundResource(R.drawable.bg_corner_one)
            tvQtyMatchingNo.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            qtyMatching = 1
        }

        tvProperPackingNo.setOnClickListener {
            tvProperPackingNo.setBackgroundResource(R.drawable.bg_corner_one_orange)
            tvProperPackingNo.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvProperPackingYes.setBackgroundResource(R.drawable.bg_corner_two)
            tvProperPackingYes.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            properPacking = 0
        }

        tvProperPackingYes.setOnClickListener {
            tvProperPackingYes.setBackgroundResource(R.drawable.bg_corner_two_green)
            tvProperPackingYes.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvProperPackingNo.setBackgroundResource(R.drawable.bg_corner_one)
            tvProperPackingNo.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            properPacking = 1
        }

        tvSKUMatchingNo.setOnClickListener {
            tvSKUMatchingNo.setBackgroundResource(R.drawable.bg_corner_one_orange)
            tvSKUMatchingNo.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvSKUMatchingYes.setBackgroundResource(R.drawable.bg_corner_two)
            tvSKUMatchingYes.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            skuMatching = 0
        }

        tvSKUMatchingYes.setOnClickListener {
            tvSKUMatchingYes.setBackgroundResource(R.drawable.bg_corner_two_green)
            tvSKUMatchingYes.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvSKUMatchingNo.setBackgroundResource(R.drawable.bg_corner_one)
            tvSKUMatchingNo.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            skuMatching = 1
        }

        tvAnyDamageItemsNo.setOnClickListener {
            tvAnyDamageItemsNo.setBackgroundResource(R.drawable.bg_corner_one_orange)
            tvAnyDamageItemsNo.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvAnyDamageItemsYes.setBackgroundResource(R.drawable.bg_corner_two)
            tvAnyDamageItemsYes.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            anyDamageItem = 0
        }

        tvAnyDamageItemsYes.setOnClickListener {
            tvAnyDamageItemsYes.setBackgroundResource(R.drawable.bg_corner_two_green)
            tvAnyDamageItemsYes.setTextColor(ContextCompat.getColor(activity, R.color.white))
            tvAnyDamageItemsNo.setBackgroundResource(R.drawable.bg_corner_one)
            tvAnyDamageItemsNo.setTextColor(ContextCompat.getColor(activity, R.color.dark_blue))
            anyDamageItem = 1
        }

        btnSubmit.setOnClickListener {
            qcChecking()
        }
    }

    private fun qcChecking() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().qcChecking(
                storeUserData.getString(Constants.USER_TOKEN),
                etQCAWBNumber.text.toString(),
                awb_id,
                qtyMatching,
                properPacking,
                skuMatching,
                anyDamageItem,
                etRemark.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                //showAlert(JSONObject(responseString).getString("message"), true)


                var alert: CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                    finish()
                })

                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun checkAWBNumber() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().prefixQCAWBNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                etQCAWBNumber.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var data = JSONObject(responseString).getJSONObject("data")
                awb_id = data.getString("id")
                /*if (data.getString("dispatch_status") == "completed") {
                    showAlert("Dispatch is completed.")
                } else if (data.getString("sts_receiving_status") == "completed") {
                    if (data.getString("qc_checking_status") == "pending") {

                    } else if (data.getString("qc_checking_status") == "completed") {
                        etQCAWBNumber.text = null
                        etQCAWBNumber.isEnabled = true
                        etQCAWBNumber.requestFocus()
                        showAlert("This QC Checking is Already Completed")
                    }
                } else {
                    etQCAWBNumber.text = null
                    etQCAWBNumber.isEnabled = true
                    etQCAWBNumber.requestFocus()
                    showAlert("Receiving is pending.")
                }*/

                if (data.getString("dispatch_status") == "completed" && data.getString("sts_receiving_status") == "completed") {
                    showAlert("Dispatch not possible.")
                } else {
                    if (data.getString("qc_checking_status") == "completed") {
                        etQCAWBNumber.text = null
                        etQCAWBNumber.isEnabled = true
                        etQCAWBNumber.requestFocus()
                        showAlert("This QC Checking is Already Completed")
                    }
                } /*else {
                    etQCAWBNumber.text = null
                    etQCAWBNumber.isEnabled = true
                    etQCAWBNumber.requestFocus()
                    showAlert("Receiving is pending.")
                }*/
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                etQCAWBNumber.text = null
                etQCAWBNumber.isEnabled = true
                etQCAWBNumber.requestFocus()
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }


}