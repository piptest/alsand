package com.alshaya.main

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.TrackPalletMDRPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_pending_request.*
import kotlinx.android.synthetic.main.activity_pending_request.btnViewPdf
import kotlinx.android.synthetic.main.activity_sign_off.*
import kotlinx.android.synthetic.main.activity_sign_off.ivBack
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.dialog_sign_box.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class PendingRequestActivity : BaseActivity() {
    var signatureImage = ""
    var name = ""
    var vehicleNumber = ""
    var signAs = ""
    var pojo = TrackPalletMDRPojo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_request)
        activity = this
        storeUserData = StoreUserData(activity)



        trackPallet_MDRId()
        Log.i("TAG", "onCreate:${pojo.type} ")

        ivBack.setOnClickListener { finish() }
        llPendingReqSignAsDispatcher.setOnClickListener {
            signAs = "SignAsDispatcher"
            signatureImage = ""
            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)
            dialog.etSignInVehicleNumber.setText(storeUserData.getString(Constants.USER_ID))
            dialog.etSignInVehicleNumber.isEnabled = false
            dialog.tvSignInName.isEnabled = false
            dialog.tvSignInName.setText(storeUserData.getString(Constants.USER_NAME))
            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = false
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    if (pojo.type == "mdr") {
                        signAsDispatcher("api/v1/sts-sign-as-dispatcher")
                    } else {
                        signAsDispatcher("api/v1/sign-as-dispatcher")
                    }
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        llPendingReqSignAsReceiver.setOnClickListener {
            signAs = "SignAsReceiver"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = false
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    if (pojo.type == "mdr") {
                        signAsDispatcher("api/v1/sts-sign-as-receiver")
                    } else {
                        signAsDispatcher("api/v1/sign-as-receiver")
                    }
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        llPendingReqSignAsSecurity.setOnClickListener {
            signAs = "SignAsSecurity"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = false
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    if (pojo.type == "mdr") {
                        signAsDispatcher("api/v1/sts-sign-as-security")
                    } else {
                        signAsDispatcher("api/v1/sign-as-security")
                    }
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }


        btnViewPdf.setOnClickListener {
            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "${Constants.URL}api/v1/generate-pdf/${
                        intent.getStringExtra("MDRId").toString()
                    }"
                )
            )
            startActivity(browserIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>, grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var required = false
        for (i in 0 until permissions.size) {
            if (grantResults[i] === PackageManager.PERMISSION_GRANTED) {
                Log.d("Permissions", "Permission Granted: " + permissions[i])
            } else if (grantResults[i] === PackageManager.PERMISSION_DENIED) {
                Log.d("Permissions", "Permission Denied: " + permissions[i])
                required = true
            }
        }
        if (required) {
            android.app.AlertDialog.Builder(activity)
                .setMessage("You need to allow access to some permissions.")
                .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        DialogInterface.OnClickListener { dialog, which ->
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", packageName, null)
                            )
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            dialog.dismiss()
                        }
                    }
                })
                .setCancelable(false)
                .create()
                .show()
        } else {
        }


    }

    private fun checkPermissions(): Boolean {
        val hasStoragePermission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
        )
        val permissions: MutableList<String> =
            ArrayList()
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        return if (permissions.isNotEmpty()) {
            requestPermissions(
                permissions.toTypedArray(),
                103
            )
            false
        } else {
            true
        }
    }

    private fun trackPallet_MDRId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().track_Pallet_MDR_id(
                storeUserData.getString(Constants.USER_TOKEN),
                intent.getStringExtra("MDRId").toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                pojo = Gson().fromJson(responseString, TrackPalletMDRPojo::class.java)

                tvPalletId.text = pojo.pallet_details.pallet_id
                tvPendingReqTotalBox.text = pojo.total_box.toString()
                llPendingReqSignAsReceiver.isVisible = pojo.pallet_details.courier_id != "1"
                if (pojo.pallet_details.sign_as_dispatcher_status == "completed") {
                    llPendingReqSignAsDispatcher.isEnabled = false
                    dispatcherStatus.isVisible = true
                    dispatcherDate.isVisible = true
                    dispatcherDate.text = pojo.pallet_details.sign_as_dispatcher_date
                } else {
                    llPendingReqSignAsDispatcher.isEnabled = true
                    dispatcherStatus.isVisible = false
                    dispatcherDate.isVisible = false
                }

                if (pojo.pallet_details.sign_as_receiver_status == "completed") {
                    llPendingReqSignAsReceiver.isEnabled = false
                    receiverStatus.isVisible = true
                    receiverDate.isVisible = true
                    receiverDate.text = pojo.pallet_details.sign_as_receiver_date
                } else {
                    llPendingReqSignAsReceiver.isEnabled = true
                    receiverStatus.isVisible = false
                    receiverDate.isVisible = false
                }

                if (pojo.pallet_details.sign_as_security_status == "completed") {
                    llPendingReqSignAsSecurity.isEnabled = false
                    securityStatus.isVisible = true
                    securityDate.isVisible = true
                    securityDate.text = pojo.pallet_details.sign_as_security_date
                } else {
                    llPendingReqSignAsSecurity.isEnabled = true
                    securityStatus.isVisible = false
                    securityDate.isVisible = false
                }
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun signAsDispatcher(url: String) {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().signAsDispatcher(
                url,
                storeUserData.getString(Constants.USER_TOKEN),
                pojo.pallet_details.id.toString(),
                name,
                vehicleNumber,
                signatureImage,
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "signAsDispatcher:$responseString")
                showAlertDispatch(JSONObject(responseString).getString("message"))
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onErrorPallet:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    fun showAlertDispatch(message: String) {
        var alert:CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setMessage(message)
        alert?.setPositiveButton("ok", View.OnClickListener {
            alert?.dismiss()
            alert = null
            trackPallet_MDRId()
        })

        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }

    /* fun getAlbumStorageDir(albumName: String?): File {
         // Get the directory for the user's public pictures directory.
         val file = File(
             Environment.getExternalStoragePublicDirectory(
                 Environment.DIRECTORY_PICTURES
             ), albumName
         )
         if (!file.mkdirs()) {
             Log.e("SignaturePad", "Directory not created")
         }
         return file
     }*/

    private fun getAlbumStorageDir(albumName: String?): File? {
        // Get the directory for the user's public pictures directory.
        val m = packageManager
        var s = packageName
        try {
            val p = m.getPackageInfo(s!!, 0)
            s = p.applicationInfo.dataDir

            val file = File(
                /*Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES
                ), albumName*/
                s
            )
            if (!file.mkdirs()) {
                Log.e("SignaturePad", "Directory not created")
            }
            return file
        } catch (e: PackageManager.NameNotFoundException) {
            Log.w("yourtag", "Error Package name not found ", e)
        }
        return null
    }

    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        val stream: OutputStream = FileOutputStream(photo)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    fun addJpgSignatureToGallery(bitmap: Bitmap): Boolean {
        /* var result = false
         try {
             val photo = File(
                 getAlbumStorageDir("SignaturePad"),
                 String.format("Signature_%d.jpg", System.currentTimeMillis())
             )
             saveBitmapToJPG(signature, photo)
             scanMediaFile(photo)

             signatureImage = photo.absolutePath

             Log.i("TAG", "addJpgSignatureToGallery:$signatureImage ")
             result = true
         } catch (e: IOException) {
             e.printStackTrace()
         }
         return result*/
        try {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
            signatureImage = Base64.encodeToString(byteArray, Base64.DEFAULT)
            return true
        } catch (e: Exception) {
            return false
        }
    }

    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri: Uri = Uri.fromFile(photo)

        mediaScanIntent.data = contentUri


        Log.i("TAG", "scanMediaFile:${mediaScanIntent.data} ")
        activity.sendBroadcast(mediaScanIntent)
    }

}