package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.alshaya.R
import com.alshaya.pojo.TrackPalletMDRPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_track_pallet.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class TrackPalletActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_pallet)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }
        etTrackMDRID.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!Utils.isEmpty(etTrackMDRID)) {
                    trackPallet_MDRId()
                }
            }
        })
        /*tvTrackMDRID.setOnClickListener {
            if (Utils.isEmpty(etTrackMDRID)) {
                showAlert("Please Enter Pallet ID / MDR ID")
            } else {
                trackPallet_MDRId()
            }
        }*/
    }

    override fun onResume() {
        super.onResume()
        etTrackMDRID.text!!.clear()
    }

    private fun trackPallet_MDRId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().track_Pallet_MDR_id(
                storeUserData.getString(Constants.USER_TOKEN),
                etTrackMDRID.text.toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var pojo = Gson().fromJson(responseString, TrackPalletMDRPojo::class.java)
                var text = etTrackMDRID.text.toString()
                startActivity(
                    Intent(activity, PendingRequestActivity::class.java).putExtra(
                        "MDRId",
                        text
                    )
                )
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                etTrackMDRID.text = null
                etTrackMDRID.requestFocus()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }


}