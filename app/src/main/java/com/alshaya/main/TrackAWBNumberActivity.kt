package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.alshaya.R
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import kotlinx.android.synthetic.main.activity_track_awbnumber.*

class TrackAWBNumberActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_awbnumber)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }

        etAWBNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!Utils.isEmpty(etAWBNumber)) {
                    var text = etAWBNumber.text.toString()
                    etAWBNumber.text = null
                    startActivity(
                        Intent(
                            activity,
                            SignOffThreeActivity::class.java
                        ).putExtra("awb_bill_number", text)
                    )

                }
            }
        })

        tvScan.setOnClickListener {
            if (Utils.isEmpty(etAWBNumber)) {
                showAlert("Please enter AWB number")
            } else {
                startActivity(
                    Intent(
                        activity,
                        SignOffThreeActivity::class.java
                    ).putExtra("awb_bill_number", etAWBNumber.text.toString())
                )

            }
        }
    }

    override fun onResume() {
        super.onResume()
        etAWBNumber.text!!.clear()
    }

}