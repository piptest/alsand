package com.alshaya.main

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.Courier
import com.alshaya.pojo.Sites
import com.alshaya.pojo.TrackPalletMDRPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_sign_off.*
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.dialog_sign_box.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.*


class SignOffActivity : BaseActivity() {
    var listCourier = ArrayList<Sites>()
    var courierId = ""
    var pojo = TrackPalletMDRPojo()
    var signatureImage = ""
    var name = ""
    var vehicleNumber = ""
    var signAs = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_off)
        activity = this
        storeUserData = StoreUserData(activity)
        trackPallet_MDRId()

        ivBack.setOnClickListener {
            finish()
        }
        tvCourier.setOnClickListener {
            signAs = ""
            signatureImage = ""
            getCouriers()
        }

        llSignAsDispatcher.setOnClickListener {
            if (courierId.isEmpty()) {
                showAlert("Please select courier.")
                return@setOnClickListener
            }
            signAs = "SignAsDispatcher"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)
            dialog.etSignInVehicleNumber.setText(storeUserData.getString(Constants.USER_ID))
            dialog.etSignInVehicleNumber.isEnabled = false
            dialog.tvSignInName.isEnabled = false
            dialog.tvSignInName.setText(storeUserData.getString(Constants.USER_NAME))
            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    signAsDispatcher("api/v1/sign-as-dispatcher")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        llSignAsReceiver.setOnClickListener {
            if (courierId.isEmpty()) {
                showAlert("Please select courier.")
                return@setOnClickListener
            }
            signAs = "SignAsReceiver"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    signAsDispatcher("api/v1/sign-as-receiver")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }


        }

        llSignAsSecurity.setOnClickListener {
            if (courierId.isEmpty()) {
                showAlert("Please select courier.")
                return@setOnClickListener
            }
            signAs = "SignAsSecurity"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    signAsDispatcher("api/v1/sign-as-security")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }


        btnViewPdf.setOnClickListener {

            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "${Constants.URL}api/v1/generate-pdf/${
                        intent.getStringExtra("palletId").toString()
                    }"
                )
            )
            startActivity(browserIntent)
        }
    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    private fun checkPermissions(): Boolean {
        val hasStoragePermission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
        )
        val permissions: MutableList<String> =
            ArrayList()
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        return if (permissions.isNotEmpty()) {
            requestPermissions(
                permissions.toTypedArray(),
                103
            )
            false
        } else {
            true
        }
    }

    private fun trackPallet_MDRId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().track_Pallet_MDR_id(
                storeUserData.getString(Constants.USER_TOKEN),
                intent.getStringExtra("palletId").toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "trackPallet_MDRId:$responseString")
                pojo = Gson().fromJson(responseString, TrackPalletMDRPojo::class.java)

                if (pojo.pallet_details.sign_as_dispatcher_status == "pending") {
                    tvSignOffCompleted.isVisible = false
                    llSignAsDispatcher.isEnabled = true
                    btnViewPdf.isVisible = false
                } else {
                    btnViewPdf.isVisible = true
                    tvSignOffCompleted.isVisible = true
                    tvSignOffDisDate.isVisible = true
                    llSignAsDispatcher.isEnabled = false
                    tvSignOffDisDate.text = pojo.pallet_details.sign_as_dispatcher_date
                }

                if (pojo.pallet_details.sign_as_receiver_status == "pending") {
                    tvSignAsRecCompleted.isVisible = false
                    llSignAsReceiver.isEnabled = true
                } else {
                    tvSignAsRecCompleted.isVisible = true
                    tvSignAsRecDate.isVisible = true
                    llSignAsReceiver.isEnabled = false
                    tvSignAsRecDate.text = pojo.pallet_details.sign_as_receiver_date

                }

                if (pojo.pallet_details.sign_as_security_status == "pending") {
                    tvSignAsSecurityCompleted.isVisible = false
                    llSignAsSecurity.isEnabled = true
                } else {
                    tvSignAsSecurityCompleted.isVisible = true
                    tvSignAsSecurityDate.isVisible = true
                    llSignAsSecurity.isEnabled = false
                    tvSignAsSecurityDate.text = pojo.pallet_details.sign_as_security_date

                }


            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun getCouriers() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().couriers(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                listCourier.clear()
                val courier = Gson().fromJson(responseString, Courier::class.java)
                listCourier.addAll(courier.data)


                val listC = ArrayList<String>()
                for (i in 0 until listCourier.size) {
                    listC.add(listCourier[i].name)
                }
                val builder = AlertDialog.Builder(activity)
                val dataAdapter = ArrayAdapter(
                    activity,
                    android.R.layout.simple_dropdown_item_1line, listC
                )
                builder.setAdapter(dataAdapter) { _, which ->
                    tvCourier.text = listCourier[which].name
                    courierId = listCourier[which].id
                    llSignAsReceiver.isVisible = listCourier[which].id != "1"
                    dispatchCourier()
                }
                val dialog = builder.create()
                dialog.show()


            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun dispatchCourier() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().dispatchCourier(
                storeUserData.getString(Constants.USER_TOKEN),
                pojo.pallet_details.id.toString(), courierId, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccessdispatchCourier:$responseString")

                var alert:CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                    /*startActivity(
                        Intent(
                            activity,
                            HomeActivity::class.java
                        ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )*/
                })

                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onErrordispatchCourier:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun signAsDispatcher(url: String) {
        Log.i("TAG", "URL:$url ")
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().signAsDispatcher(
                url,
                storeUserData.getString(Constants.USER_TOKEN),
                pojo.pallet_details.id.toString(),
                name,
                vehicleNumber,
                signatureImage, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "signAsDispatcher:$responseString")
                showAlertDispatch(JSONObject(responseString).getString("message"))
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    fun showAlertDispatch(message: String) {
        var alert:CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setMessage(message)
        alert?.setPositiveButton("ok", View.OnClickListener {
            alert?.dismiss()
            alert = null
            trackPallet_MDRId()
        })
        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>, grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var required = false
        for (i in 0 until permissions.size) {
            if (grantResults[i] === PackageManager.PERMISSION_GRANTED) {
                Log.d("Permissions", "Permission Granted: " + permissions[i])
            } else if (grantResults[i] === PackageManager.PERMISSION_DENIED) {
                Log.d("Permissions", "Permission Denied: " + permissions[i])
                required = true
            }
        }
        if (required) {
            android.app.AlertDialog.Builder(activity)
                .setMessage("You need to allow access to some permissions.")
                .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        DialogInterface.OnClickListener { dialog, which ->
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", packageName, null)
                            )
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            dialog.dismiss()
                        }
                    }
                })
                .setCancelable(false)
                .create()
                .show()
        } else {
        }


    }

    private fun getAlbumStorageDir(albumName: String?): File? {
        // Get the directory for the user's public pictures directory.
        val m = packageManager
        var s = packageName
        try {
            val p = m.getPackageInfo(s!!, 0)
            s = p.applicationInfo.dataDir

            val file = File(
                /*Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES
                ), albumName*/
                s
            )
            if (!file.mkdirs()) {
                Log.e("SignaturePad", "Directory not created")
            }
            return file
        } catch (e: PackageManager.NameNotFoundException) {
            Log.w("yourtag", "Error Package name not found ", e)
        }
        return null
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }


    private fun addJpgSignatureToGallery(bitmap: Bitmap): Boolean {
        /*var result = false
        try {
            val photo = File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("Signature_%d.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature!!, photo)
            scanMediaFile(photo)
            signatureImage = photo.absolutePath
            // signAsDispatcher()

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result*/
        try {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
            signatureImage = Base64.encodeToString(byteArray, Base64.DEFAULT)
            return true
        } catch (e: Exception) {
            return false
        }
    }


    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri: Uri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        activity.sendBroadcast(mediaScanIntent)
    }


}