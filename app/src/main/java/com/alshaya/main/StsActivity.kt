package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import com.alshaya.R
import com.alshaya.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_sts.*

class StsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sts)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }

        btnSTSReceiving.setOnClickListener {
            startActivity(Intent(activity, STSReceivingActivity::class.java))
        }

        btnSTSDispatch.setOnClickListener {
            startActivity(Intent(activity, STSDispatchActivity::class.java))
        }
    }
}