package com.alshaya.main

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import com.alshaya.controls.CEditText
import com.alshaya.controls.CTextView
import com.alshaya.controls.CustomDialog
import com.alshaya.controls.CustomProgressDialog
import com.alshaya.utils.BetterActivityResult
import com.alshaya.utils.StoreUserData


open class BaseActivity : AppCompatActivity() {
    lateinit var activity: AppCompatActivity
    lateinit var storeUserData: StoreUserData
    private lateinit var progressDialog: CustomProgressDialog
    lateinit var alert: CustomDialog
    protected val activityLauncher: BetterActivityResult<Intent, ActivityResult> =
        BetterActivityResult.registerActivityForResult(this)


    fun showAlert(message: String) {
        if (this::alert.isInitialized && alert.isShowing) {
            alert.dismiss()
        }
        alert = CustomDialog(activity)
        alert.setCancelable(false)
        alert.show()
        alert.setMessage(message)
        alert.setPositiveButton("ok", View.OnClickListener {
            alert.dismiss()
        })
    }

    fun showAlert(message: String, finish: Boolean) {
        if (this::alert.isInitialized && alert.isShowing) {
            alert.dismiss()
        }
        alert = CustomDialog(activity)
        alert.setCancelable(false)
        alert.show()
        alert.setMessage(message)
        alert.setPositiveButton("ok", View.OnClickListener {
            alert.dismiss()
            if (finish) {
                finish()
            }
        })
    }

    fun showAlert(message: String, finish: Boolean, java: Class<*>) {
        if (this::alert.isInitialized && alert.isShowing) {
            alert.dismiss()
        }
        alert = CustomDialog(activity)
        alert.setCancelable(false)
        alert.show()
        alert.setMessage(message)
        alert.setPositiveButton("ok", View.OnClickListener {
            alert.dismiss()
            if (finish) {
                startActivity(Intent(activity, java))
            }
        })
    }


    fun showAlert(title: String, message: String) {
        if (this::alert.isInitialized && alert.isShowing) {
            alert.dismiss()
        }
        alert = CustomDialog(activity)
        alert.setCancelable(false)
        alert.show()
        alert.setTitle(title)
        alert.setMessage(message)
        alert.setPositiveButton("ok") {
            alert.dismiss()
        }
    }

    fun showProgress() {
        progressDialog = CustomProgressDialog(activity)
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    fun dismissProgress() {
        if (this::progressDialog.isInitialized)
            progressDialog.dismiss()
    }

    fun showProgress(message: String) {
        progressDialog = CustomProgressDialog(activity)
        progressDialog.setCancelable(false)
        progressDialog.setTitle(message)
        progressDialog.show()
    }

    fun setWatcher(currentEditText: CEditText, nextEditText: CEditText?, btnScan: CTextView) {

        currentEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                currentEditText.isEnabled = false
                nextEditText?.requestFocus()
            }

        })
        btnScan.setOnClickListener {
            currentEditText.setText("")
            currentEditText.isEnabled = true
            currentEditText.requestFocus()
        }
    }
}