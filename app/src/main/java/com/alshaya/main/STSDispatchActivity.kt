package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.adapter.STSDispatchAdapter
import com.alshaya.controls.CustomDialog
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import kotlinx.android.synthetic.main.activity_stsdispatch.*
import kotlinx.android.synthetic.main.custom_dialog.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class STSDispatchActivity : BaseActivity() {
    var list = ArrayList<String>()
    lateinit var adapter: STSDispatchAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stsdispatch)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener { finish() }
        adapter = STSDispatchAdapter(activity, list)

        rvSTSDispatch.adapter = adapter

        etMDRId.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (Utils.isEmpty(etMDRId)) {
                    return
                }
                if (etMDRId.text.toString().length == 12) {
                    etMDRId.isEnabled = false
                    checkMRDID()
                } else {
                    showAlert("Invalid MDR Id. Please enter 12 Character MDR id.")
                    etMDRId.text = null
                }
            }

        })
        etAWBBillNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!Utils.isEmpty(etAWBBillNumber)) {
                    etAWBBillNumber.isEnabled = false
                    checkAWBNumber()
                }
            }
        })
        scanMdr.setOnClickListener {
            etMDRId.setText("")
            etMDRId.isEnabled = true
            etMDRId.requestFocus()
        }

        scanAwbBillNumber.setOnClickListener {
            etAWBBillNumber.setText("")
            etAWBBillNumber.isEnabled = true
            etAWBBillNumber.requestFocus()
        }

        /*scanAwbBillNumber.setOnClickListener {
            if (Utils.isEmpty(etAWBBillNumber)) {
                "please enter AWB bill number"
            } else {
                checkAWBNumber()
            }
        }*/

        ivListDelete.setOnClickListener {
            deleteLastAwb()
        }

        btnCompletedDispatch.setOnClickListener {
            if (list.isNotEmpty()) {
                stsAddDispatch()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        etMDRId.text = null
        etAWBBillNumber.text = null
        tvLastAEB.text = null
    }

    var mdrId = ""
    var awbId = ""
    private fun checkMRDID() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().checkMRDID(
                storeUserData.getString(Constants.USER_TOKEN),
                etMDRId.text.toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                list.clear()
                if (JSONObject(responseString).getInt("is_available") == 1) {
                    etMDRId.isEnabled = false
                    etAWBBillNumber.isEnabled = true
                    etAWBBillNumber?.requestFocus()

                    var json = JSONObject(responseString)
                    var array = json.getJSONArray("data")
                    for (i in 0 until array.length()) {
                        var jsonObj = array.getJSONObject(i)
                        list.add(jsonObj.getString("awb_number"))

                    }
                    if (array.length() > 0) {
                        tvLastAEB.text = array.getJSONObject(0).getString("awb_number")
                        tvTotalBox.text = "Total Box ${list.size}"
                        awbId = array.getJSONObject(0).getString("awb_id")
                        ivListDelete.isVisible =
                            array.getJSONObject(0).getInt("allowed_delete") == 1

                        mdrId = json.optString("mdr_id")
                    } else {
                        mdrId = ""
                        ivListDelete.isVisible = false
                    }

                } else {
                    etMDRId.isEnabled = true
                    etMDRId.text = null
                    etMDRId.requestFocus()
                    showAlert(JSONObject(responseString).optString("message"))
                }
                adapter.notifyDataSetChanged()
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                etMDRId.isEnabled = true
                etMDRId.text = null
                etMDRId.requestFocus()
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun checkAWBNumber() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().checkAWBNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                etAWBBillNumber.text.toString(),
                etMDRId.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                var data = JSONObject(responseString)
                mdrId = data.getString("mdr_id")
                etAWBBillNumber.text = null
                etAWBBillNumber.isEnabled = true
                etAWBBillNumber.requestFocus()
                checkMRDID()
                /*if (data.getString("sts_dispatch_status") == "pending") {
                    if (list.contains(etAWBBillNumber.text.toString())) {
                        showAlert("Already added")
                    } else {
                        // list.add(etAWBBillNumber.text.toString())

                        etAWBBillNumber.text = null
                        etAWBBillNumber.isEnabled = true
                        etAWBBillNumber.requestFocus()
                        adapter.notifyDataSetChanged()
                    }
                } else if (data.getString("sts_dispatch_status") == "completed") {
                    showAlert("This STS Dispatch is already completed")
                }*/

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                etAWBBillNumber.isEnabled = true
                etAWBBillNumber.text = null
                etAWBBillNumber.requestFocus()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
                checkMRDID()
            }
        })
    }

    private fun deleteLastAwb() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stsDispatchLastDelete(
                storeUserData.getString(Constants.USER_TOKEN),
                awbId,
                mdrId, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccessAddDispatch:$responseString")
                showAlertDispatch(JSONObject(responseString).getString("message"))
                checkMRDID()
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun stsAddDispatch() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stsAddDispatch(
                storeUserData.getString(Constants.USER_TOKEN),
                etMDRId.text.toString(),
                list, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccessAddDispatch:$responseString")
                showAlertDispatch(JSONObject(responseString).getString("message"))

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    fun showAlertDispatch(message: String) {
        var alert: CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setMessage(message)
        alert?.setPositiveButton("ok", View.OnClickListener {
            alert?.dismiss()
            alert = null
            Log.i("TAG", "showAlertDispatch: " + etMDRId.text.toString())
            startActivity(
                Intent(
                    activity,
                    STSDispatchSignOffActivity::class.java
                ).putExtra("PalletID", etMDRId.text.toString())
            )
        })

        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }
}