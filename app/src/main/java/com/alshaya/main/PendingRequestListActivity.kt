package com.alshaya.main

import android.os.Bundle
import android.util.Log
import com.alshaya.R
import com.alshaya.adapter.PendingRequestAdapter
import com.alshaya.pojo.PendingRequest
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_pending_request_list.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response


class PendingRequestListActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_request_list)
        activity = this
        storeUserData = StoreUserData(activity)
        getPendingRequest()
        ivBack.setOnClickListener { finish() }
    }


    private fun getPendingRequest() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().getPendingRequest(
                storeUserData.getString(Constants.USER_TOKEN),
                "all",
                        storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                var pojo = Gson().fromJson(responseString, PendingRequest::class.java)
                recyclerView.adapter = PendingRequestAdapter(activity, pojo.data)

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }
}