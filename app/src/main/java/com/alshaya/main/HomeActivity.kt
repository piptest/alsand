package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.alshaya.R
import com.alshaya.adapter.HomeAdapter
import com.alshaya.pojo.Home
import com.alshaya.pojo.Profile
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class HomeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        activity = this
        storeUserData = StoreUserData(activity)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        getHome()
        getProfile()
        ivBack.setOnClickListener {
            finish()
        }
        ivProfile.setOnClickListener {
            startActivity(Intent(activity, MyProfileActivity::class.java))
        }

    }

    private fun getProfile() {
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().profile(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                val profile = Gson().fromJson(responseString, Profile::class.java).data
                if (profile.image != null) {
                    storeUserData.setString(Constants.USER_IMAGE, profile.image.toString())
                    Glide.with(activity).load(Constants.URL + profile.image).circleCrop()
                        .into(ivProfile)
                } else {
                    Glide.with(activity).load(R.drawable.default_profile).circleCrop()
                        .into(ivProfile)
                }
                storeUserData.setString(Constants.USER_NAME, profile.name)
                tvUserName.text = "Hi, ${profile.name}"
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun getHome() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().home(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                val json = JSONObject(responseString).getJSONObject("permission")
                val list = ArrayList<Home>()
                if (json.getInt("packing") == 1) {
                    list.add(Home(R.drawable.packing, "Packing"))
                }
                if (json.getInt("sagregation") == 1) {
                    list.add(Home(R.drawable.segragation, "Segregation"))
                }
                if (json.getInt("dispatch") == 1) {
                    list.add(Home(R.drawable.dispatches, "Dispatch"))
                }
                if (json.getInt("sts") == 1) {
                    list.add(Home(R.drawable.s_t_s, "STS"))
                }
                if (json.getInt("pending-request") == 1) {
                    list.add(Home(R.drawable.packing, "Track / Pending Request"))
                }
                if (json.getInt("qc-checking") == 1) {
                    list.add(Home(R.drawable.qr_checking, "QC Checking"))
                }
                rvHome.adapter = HomeAdapter(activity, list)
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }
}