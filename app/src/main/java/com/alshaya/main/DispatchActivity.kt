package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.alshaya.R
import com.alshaya.adapter.DispatchAdapter
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.ScanPalletIdPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_dispatch.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class DispatchActivity : BaseActivity() {
    lateinit var adapter: DispatchAdapter
    var list = ArrayList<ScanPalletIdPojo.BoxDataBean>()
    var pojo = ScanPalletIdPojo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispatch)
        activity = this
        storeUserData = StoreUserData(activity)
        alert = CustomDialog(activity)
        ivBack.setOnClickListener {
            finish()
        }

        tvPalletId.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (Utils.isEmpty(tvPalletId)) {
                    return
                }
                if (tvPalletId.text.toString().length == 12) {
                    tvPalletId.isEnabled = false
                    etScanNumber?.requestFocus()
                    scanPalletId()
                } else {
                    showAlert("Invalid Pallet Id. Please enter 12 Character Pallet id.")
                    tvPalletId.text = null
                }
            }
        })
        scanPalletId.setOnClickListener {
            tvPalletId.setText("")
            tvPalletId.isEnabled = true
            tvPalletId.requestFocus()
        }

        scanAwbNumber.setOnClickListener {
            etScanNumber.text = null
            etScanNumber.requestFocus()
        }

        etScanNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                var check = false
                etScanNumber.isEnabled = false
                if (etScanNumber.text.toString().isNotEmpty()) {

                    dispatchAwbNumber()
                    /*for (i in list) {
                        if (i.awb_bill_number == etScanNumber.text.toString()) {
                            if (i.isFirst == 0) {
                                etScanNumber.text = null
                                etScanNumber.isEnabled = true
                                etScanNumber.requestFocus()
                                check = true
                                i.isScanned = 1
                                i.isFirst = 1
                                pojo.data.total_box--
                                pojo.data.scannedBox++
                                pojo.data.totalScanned++
                                tvBoxPending.text = pojo.data.total_box.toString()
                                tvDisScannedBox.text = pojo.data.scannedBox.toString()
                                adapter.notifyDataSetChanged()
                            }
                        }
                    }
                    if (!check) {
                        etScanNumber.text = null
                        etScanNumber.isEnabled = false
                        if (alert.isShowing) {
                            alert.dismiss()
                        }
                        alert.show()
                        alert.setMessage("Invalid AWB Number.")
                        alert.setPositiveButton("OK") {
                            etScanNumber.text = null
                            etScanNumber.isEnabled = true
                            etScanNumber.requestFocus()
                            alert.dismiss()
                        }
                    }*/
                }
            }
        })

        btnSubmit.setOnClickListener {
            if (list.size == pojo.data.scannedBox) {
                addDispatch()
            } else {
                showAlert("Please scanned all items")
            }
        }
        adapter =
            DispatchAdapter(activity, list)
        Handler(Looper.getMainLooper()).postDelayed({
            Utils.hideKB(activity, tvPalletId)
        }, 300)

    }


    override fun onResume() {
        super.onResume()
        tvPalletId.text!!.clear()
        etScanNumber.text!!.clear()
        tvDispatchTotalBox.text=null
        tvBoxPending.text=null
        tvDisScannedBox.text=null
        rvAWBNumber.adapter=null
        list.clear()

    }


    private fun dispatchAwbNumber() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().dispatchAwbNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                pojo.data.id,
                etScanNumber.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                /*pojo = Gson().fromJson(responseString, ScanPalletIdPojo::class.java)
                list.clear()
                list.addAll(pojo.data.box_data)
                tvDispatchTotalBox.text = pojo.data.total_box.toString()
                tvBoxPending.text = pojo.data.total_box.toString()
                rvAWBNumber.adapter = adapter
                adapter.notifyDataSetChanged()*/

                etScanNumber.text = null
                etScanNumber.isEnabled = true
                etScanNumber.requestFocus()
                scanPalletId()

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                etScanNumber.text = null
                etScanNumber.isEnabled = true
                etScanNumber.requestFocus()
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun scanPalletId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().scanPalletId(
                storeUserData.getString(Constants.USER_TOKEN),
                tvPalletId.text.toString()
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                pojo = Gson().fromJson(responseString, ScanPalletIdPojo::class.java)
                list.clear()
                list.addAll(pojo.data.box_data)
                tvDispatchTotalBox.text = pojo.data.total_box.toString()
                rvAWBNumber.adapter = adapter

                for (i in 0 until list.size) {
                    if (list[i].dispatch_status == "completed") {
                        list[i].isScanned = 1
                        list[i].isFirst = 1
                        pojo.data.scannedBox++
                    }
                }
                tvBoxPending.text = (pojo.data.total_box - pojo.data.scannedBox).toString()
                tvDisScannedBox.text = pojo.data.scannedBox.toString()
                adapter.notifyDataSetChanged()

                if (list.size == pojo.data.scannedBox) {
                    addDispatch()
                }
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                tvPalletId.text = null
                tvPalletId.isEnabled = true
                tvPalletId.requestFocus()
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    //3
    private fun addDispatch() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().addDispatch(
                storeUserData.getString(Constants.USER_TOKEN),
                pojo.data.id.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                //  showAlert(JSONObject(responseString).getString("message"))

                startActivity(
                    Intent(activity, SignOffActivity::class.java)
                        .putExtra("palletId", tvPalletId.text.toString())
                )
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }


    interface OnDataChange {
        fun onItemsChange()
    }

    var onItemChange = object : OnDataChange {
        override fun onItemsChange() {


        }


    }
}