package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import com.alshaya.R
import com.alshaya.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_track.*

class TrackActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }

        btnTrackAWBNumber.setOnClickListener {
            startActivity(Intent(activity, TrackAWBNumberActivity::class.java))
        }
        btnTrackPalletId.setOnClickListener {
            startActivity(Intent(activity, TrackPalletActivity::class.java))
        }
        pendingRequest.setOnClickListener {
            startActivity(Intent(activity, PendingRequestListActivity::class.java))
        }

    }
}