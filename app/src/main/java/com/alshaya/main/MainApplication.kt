package com.alshaya.main

import android.app.Application
import android.os.StrictMode

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
    }
}