package com.alshaya.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.alshaya.R
import com.alshaya.adapter.DispatchAdapter
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.ScanPalletIdPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_stsreceiving.*
import kotlinx.android.synthetic.main.custom_dialog.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class STSReceivingActivity : BaseActivity() {
    var list = ArrayList<ScanPalletIdPojo.BoxDataBean>()
    var pojo = ScanPalletIdPojo()
    lateinit var adapter: DispatchAdapter
    var dispatchID = ""
    var allSubmitted = false
    var completeScanList = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stsreceiving)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }
        etSTSRecPalletID.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (Utils.isEmpty(etSTSRecPalletID)) {
                    return
                }
                if (etSTSRecPalletID.text.toString().length == 12) {
                    etSTSRecPalletID.isEnabled = false
                    scanPalletId()
                } else {
                    showAlert("Invalid Pallet Id. Please enter 12 Character Pallet id.")
                    etSTSRecPalletID.text = null
                }
            }
        })
        scanPalletId.setOnClickListener {
            etSTSRecPalletID.text = null
            etSTSRecPalletID.isEnabled = true
            etSTSRecPalletID.requestFocus()
        }

        btnSubmitSTSReceiving.setOnClickListener {
            /*  if (list.size == pojo.data.totalScanned) {

              } else {
                  showAlert("Please scanned all items")
              }*/
            stsReceiving()
        }

        btnCompletedTheReceiving.setOnClickListener {
            showAlertData("Scan all data successFully")
        }

        if (allSubmitted) {
            btnCompletedTheReceiving.setBackgroundResource(R.drawable.btn_bg)
            btnCompletedTheReceiving.isEnabled = true
        }


        adapter =
            DispatchAdapter(activity, list)

        etSTSRecScan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {

                if (etSTSRecScan.text.toString().isNotEmpty()) {
                    /*var check = true
                    for (i in list) {
                        if (i.awb_bill_number == etSTSRecScan.text.toString()) {
                            if (i.isFirst == 0) {
                                etSTSRecScan.text = null
                                i.isScanned = 1
                                i.isFirst = 1
                                completeScanList.add(i.awb_bill_number)
                                Log.i("TAG", "completeScanList:$completeScanList ")
                                pojo.data.total_box--
                                pojo.data.scannedBox++
                                tvSTSRecPending.text = pojo.data.total_box.toString()
                                tvSTSRecScanned.text = pojo.data.scannedBox.toString()
                                adapter.notifyDataSetChanged()
                                check = false
                            }
                        }
                    }
                    if (check) {
                        showAlert("Invalid awb number.")
                        etSTSRecScan.text = null
                    }*/
                    receiveAwb()
                }
            }
        })

    }

    private fun scanPalletId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stsReceivingPalletId(
                storeUserData.getString(Constants.USER_TOKEN),
                etSTSRecPalletID.text.toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                pojo = Gson().fromJson(responseString, ScanPalletIdPojo::class.java)
                dispatchID = pojo.data.id.toString()
                list.clear()
                list.addAll(pojo.data.box_data)

                for (i in list) {
                    if (i.sts_receiving_status == "completed") {
                        completeScanList.add(i.awb_bill_number)
                        i.isScanned = 1
                        i.isFirst = 1
                        pojo.data.total_box--
                        pojo.data.scannedBox++
                    }
                }

                Log.i("TAG", "completeScanList:$completeScanList ")

                tvTotalBox.text = pojo.data.total_box.toString()
                tvSTSRecPending.text = pojo.data.total_box.toString()

                if (list.size == pojo.data.scannedBox) {
                    //btnCompletedTheReceiving.isEnabled = true

                } else {
                    etSTSRecScan.requestFocus()
                }
                rvSTSReceiving.adapter = adapter
                adapter.notifyDataSetChanged()

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                etSTSRecPalletID.text = null
                etSTSRecPalletID.isEnabled = true
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun receiveAwb() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stsReceiveingAwbNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                dispatchID,
                etSTSRecScan.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                /*pojo = Gson().fromJson(responseString, ScanPalletIdPojo::class.java)
                dispatchID = pojo.data.id.toString()
                list.clear()
                list.addAll(pojo.data.box_data)

                for (i in list) {
                    if (i.sts_receiving_status == "completed") {
                        completeScanList.add(i.awb_bill_number)
                        i.isScanned = 1
                        i.isFirst = 1
                        pojo.data.total_box--
                        pojo.data.scannedBox++
                    }
                }

                Log.i("TAG", "completeScanList:$completeScanList ")

                tvTotalBox.text = pojo.data.total_box.toString()
                tvSTSRecPending.text = pojo.data.total_box.toString()

                if (list.size == pojo.data.scannedBox) {
                    btnCompletedTheReceiving.isEnabled = true
                } else {
                    etSTSRecScan.requestFocus()
                }
                rvSTSReceiving.adapter = adapter
                adapter.notifyDataSetChanged()*/

                if (JSONObject(responseString).getString("message") != "true") {
                    var alert: CustomDialog? = CustomDialog(activity)
                    alert?.setCancelable(false)
                    alert?.show()
                    alert?.setMessage(JSONObject(responseString).getString("message"))
                    alert?.setPositiveButton("ok", View.OnClickListener {
                        alert?.dismiss()
                        alert = null
                        finish()
                    })

                    Handler(Looper.getMainLooper()).postDelayed({
                        alert?.btnPositive?.performClick()
                    }, 1500)
                } else {
                    etSTSRecScan.text = null
                    etSTSRecScan.isEnabled = true
                    etSTSRecScan.requestFocus()
                    scanPalletId()
                }
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                etSTSRecScan.text = null
                etSTSRecScan.isEnabled = true
                etSTSRecScan.requestFocus()
                scanPalletId()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun stsReceiving() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stsReceiving(
                storeUserData.getString(Constants.USER_TOKEN),
                dispatchID,
                completeScanList, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                if (list.size == pojo.data.scannedBox) {
                    btnCompletedTheReceiving.setBackgroundResource(R.drawable.btn_bg)
                    btnCompletedTheReceiving.isEnabled = true
                    etSTSRecScan.text = null
                    etSTSRecScan.isEnabled = true
                    etSTSRecScan.requestFocus()

                    tvTotalBox.text = null
                    tvSTSRecPending.text = null
                    tvSTSRecScanned.text = null
                    list.clear()
                    adapter.notifyDataSetChanged()
                }

                var alert: CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                })

                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)


            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    fun showAlertData(message: String) {
        var alert: CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setTitle(title)
        alert?.setMessage(message)
        alert?.setPositiveButton("ok") {
            alert?.dismiss()
            alert = null
            finish()
        }
        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }


}