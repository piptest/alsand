package com.alshaya.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.alshaya.BuildConfig
import com.alshaya.R
import com.alshaya.pojo.Profile
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_my_profile.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response


class MyProfileActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)
        activity = this
        storeUserData = StoreUserData(activity)
        getProfile()

        appVersion.text = "Version: ${BuildConfig.VERSION_NAME}"
        ivBackProfile.setOnClickListener {
            finish()
        }

        tvLogout.setOnClickListener {
            showLogoutAlert()
        }

    }

    private fun showLogoutAlert() {
        AlertDialog.Builder(activity)
            .setMessage("Are you sure you want to logout?")
            .setPositiveButton("Yes") { _, _ ->
                storeUserData.clearData()
                activity.startActivity(
                    Intent(activity, LoginActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
                finish()

            }
            .setNegativeButton("No") { dialogInterface, _ -> dialogInterface.dismiss() }
            .create()
            .show()
    }

    private fun getProfile() {
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().profile(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                val profile = Gson().fromJson(responseString, Profile::class.java).data
                if (profile.image != null) {
                    storeUserData.setString(Constants.USER_IMAGE, profile.image.toString())
                    Glide.with(activity).load(Constants.URL + profile.image).circleCrop()
                        .into(imgProfile)
                } else {
                    Glide.with(activity).load(R.drawable.default_profile).circleCrop()
                        .into(imgProfile)
                }
                tvEmployeeId.text = profile.login_id
                tvName.text = profile.name
                tvEmail.text = profile.email
                tvPhone.text = profile.mobile_no

                llCallOperation.setOnClickListener {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:${profile.call_operation_team}")
                    startActivity(intent)
                }
                llChatOnWhatsapp.setOnClickListener {
                    val url = "https://api.whatsapp.com/send?phone=${profile.chat_on_whatsapp}"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
                llEmailOperation.setOnClickListener {
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/html"
                    intent.putExtra(Intent.EXTRA_EMAIL, profile.email_operation_team)
                    intent.putExtra(Intent.EXTRA_SUBJECT, "")
                    intent.putExtra(Intent.EXTRA_TEXT, "")
                    startActivity(Intent.createChooser(intent, "Send Email"))
                }

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }
}