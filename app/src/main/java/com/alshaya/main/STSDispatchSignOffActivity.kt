package com.alshaya.main

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.Courier
import com.alshaya.pojo.Sites
import com.alshaya.pojo.TrackPalletMDRPojo
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_stsdispatch_sign_off.*
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.dialog_sign_box.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.*


class STSDispatchSignOffActivity : BaseActivity() {
    var listStore = ArrayList<Sites>()
    var storeId = ""
    var signatureImage = ""
    var signAS = ""
    var name = ""
    var vehicleNumber = ""
    var dispatchID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_stsdispatch_sign_off)
        ivBack.setOnClickListener { finish() }
        trackPallet_MDRId()
        tvStore.setOnClickListener {
            signAS = ""
            signatureImage = ""
            getStores()
        }
        llSignAsDispatcher2.setOnClickListener {
            if (storeId.isEmpty()) {
                showAlert("Please select store.")
                return@setOnClickListener
            }
            signAS = "SignAsDispatcher"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)
            dialog.tvEmployee.setText("Employee Id")
            dialog.etSignInVehicleNumber.setHint("Employee Id")
            dialog.etSignInVehicleNumber.setText(storeUserData.getString(Constants.USER_ID))
            dialog.etSignInVehicleNumber.isEnabled = false
            dialog.tvSignInName.isEnabled = false
            dialog.tvSignInName.setText(storeUserData.getString(Constants.USER_NAME))
            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    STSSignAsDispatcher("api/v1/sts-sign-as-dispatcher")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }

        llSignAsReceiver2.setOnClickListener {
            if (storeId.isEmpty()) {
                showAlert("Please select store.")
                return@setOnClickListener
            }
            signAS = "SignAsReceiver"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    STSSignAsDispatcher("api/v1/sts-sign-as-receiver")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }


        }

        llSignAsSecurity2.setOnClickListener {
            if (storeId.isEmpty()) {
                showAlert("Please select store.")
                return@setOnClickListener
            }
            signAS = "SignAsSecurity"
            signatureImage = ""

            val dialog = Dialog(activity)
            val view = layoutInflater.inflate(R.layout.dialog_sign_box, null)
            dialog.setContentView(view)
            dialog.show()
            dialog.setCancelable(false)

            val mSignaturePad = view.findViewById<SignaturePad>(R.id.signaturePad)
            mSignaturePad!!.setOnSignedListener(object : SignaturePad.OnSignedListener {
                override fun onStartSigning() {
                }

                override fun onSigned() {
                    dialog.btnDialogSave!!.isEnabled = true
                    dialog.btnDialogSave.setBackgroundResource(R.drawable.btn_bg)
                    dialog.btnDialogCancel!!.isEnabled = true
                }

                override fun onClear() {
                    dialog.btnDialogSave!!.isEnabled = false
                    dialog.btnDialogCancel!!.isEnabled = true
                }
            })

            dialog.btnDialogSave.setOnClickListener {
                if (!dialog.tvSignInName.text.isNullOrEmpty()) {
                    name = dialog.tvSignInName.text.toString()

                }
                if (!dialog.etSignInVehicleNumber.text.isNullOrEmpty()) {
                    vehicleNumber = dialog.etSignInVehicleNumber.text.toString()
                }
                val signatureBitmap = mSignaturePad.signatureBitmap
                if (addJpgSignatureToGallery(signatureBitmap)) {
                    STSSignAsDispatcher("api/v1/sts-sign-as-security")
                } else {
                    Toast.makeText(
                        activity,
                        "Unable to store the signature",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                dialog.dismiss()
            }
            dialog.btnDialogCancel.setOnClickListener {
                dialog.dismiss()
            }
        }


        btnViewPdf.setOnClickListener {

            val browserIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "${Constants.URL}api/v1/generate-pdf/${
                        intent.getStringExtra("PalletID").toString()
                    }"
                )
            )
            startActivity(browserIntent)
        }


    }

    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    private fun checkPermissions(): Boolean {
        val hasStoragePermission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
        )
        val permissions: MutableList<String> =
            ArrayList()
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        return if (permissions.isNotEmpty()) {
            requestPermissions(
                permissions.toTypedArray(),
                103
            )
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>, grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var required = false
        for (i in 0 until permissions.size) {
            if (grantResults[i] === PackageManager.PERMISSION_GRANTED) {
                Log.d("Permissions", "Permission Granted: " + permissions[i])
            } else if (grantResults[i] === PackageManager.PERMISSION_DENIED) {
                Log.d("Permissions", "Permission Denied: " + permissions[i])
                required = true
            }
        }
        if (required) {
            android.app.AlertDialog.Builder(activity)
                .setMessage("You need to allow access to some permissions.")
                .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        DialogInterface.OnClickListener { dialog, which ->
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", packageName, null)
                            )
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            dialog.dismiss()
                        }
                    }
                })
                .setCancelable(false)
                .create()
                .show()
        } else {
        }

    }

    private fun trackPallet_MDRId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().track_Pallet_MDR_id(
                storeUserData.getString(Constants.USER_TOKEN),
                intent.getStringExtra("PalletID").toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var pojo = Gson().fromJson(responseString, TrackPalletMDRPojo::class.java)
                dispatchID = pojo.pallet_details.id.toString()
                if (pojo.pallet_details.sign_as_dispatcher_status == "completed") {
                    llSignAsDispatcher2.isEnabled = false
                    tvSignOffCompleted2.isVisible = true
                    tvSignOffDisDate2.isVisible = true
                    btnViewPdf.isVisible = true
                    tvSignOffDisDate2.text = pojo.pallet_details.sign_as_dispatcher_date
                } else {
                    btnViewPdf.isVisible = false
                    tvSignOffCompleted2.isVisible = false
                    tvSignOffDisDate2.isVisible = false

                }

                if (pojo.pallet_details.sign_as_receiver_status == "completed") {
                    llSignAsReceiver2.isEnabled = false
                    tvSignAsRecCompleted2.isVisible = true
                    tvSignAsRecDate2.isVisible = true
                    tvSignAsRecDate2.text = pojo.pallet_details.sign_as_receiver_date
                } else {
                    tvSignAsRecCompleted2.isVisible = false
                    tvSignAsRecDate2.isVisible = false
                }

                if (pojo.pallet_details.sign_as_security_status == "completed") {
                    llSignAsSecurity2.isEnabled = false
                    tvSignAsSecurityCompleted2.isVisible = true
                    tvSignAsSecurityDate2.isVisible = true
                    tvSignAsSecurityDate2.text = pojo.pallet_details.sign_as_security_date
                } else {
                    tvSignAsSecurityCompleted2.isVisible = false
                    tvSignAsSecurityDate2.isVisible = false
                }

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun getStores() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().stores(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                listStore.clear()
                val courier = Gson().fromJson(responseString, Courier::class.java)
                listStore.addAll(courier.data)


                val listC = ArrayList<String>()
                for (i in 0 until listStore.size) {
                    listC.add(listStore[i].name)
                }
                val builder = AlertDialog.Builder(activity)
                val dataAdapter = ArrayAdapter(
                    activity,
                    android.R.layout.simple_dropdown_item_1line, listC
                )
                builder.setAdapter(dataAdapter) { _, which ->
                    tvStore.text = listStore[which].name
                    storeId = listStore[which].id
                    dispatchStore(false)

                }
                val dialog = builder.create()
                dialog.show()

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun dispatchStore(isFinish: Boolean) {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().dispatchStore(
                storeUserData.getString(Constants.USER_TOKEN),
                dispatchID, storeId, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var alert: CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                    if (isFinish)
                        startActivity(
                            Intent(
                                activity,
                                HomeActivity::class.java
                            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        )
                })
                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun STSSignAsDispatcher(url: String) {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().STSsignAsDispatcher(
                url,
                storeUserData.getString(Constants.USER_TOKEN),
                dispatchID,
                name,
                vehicleNumber,
                signatureImage, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "signAsDispatcher:$responseString")
                showAlertDispatch(JSONObject(responseString).getString("message"))


            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }


    fun showAlertDispatch(message: String) {
        var alert: CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setMessage(message)
        alert?.setPositiveButton("ok", View.OnClickListener {
            alert?.dismiss()
            alert = null
            trackPallet_MDRId()
        })


        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }

    /*private fun getAlbumStorageDir(albumName: String?): File? {
        // Get the directory for the user's public pictures directory.
        val file = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
        )
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created")
        }
        return file
    }*/

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    private fun addJpgSignatureToGallery(bitmap: Bitmap): Boolean {
        /*var result = false
        try {
            val photo = File(
                getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                String.format("Signature_%d.jpg", System.currentTimeMillis())
            )
            saveBitmapToJPG(signature!!, photo)
            scanMediaFile(photo)
            signatureImage = photo.absolutePath
            // signAsDispatcher()

            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result*/
        try {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
            signatureImage = Base64.encodeToString(byteArray, Base64.DEFAULT)
            return true
        } catch (e: Exception) {
            return false
        }
    }

    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri: Uri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        activity.sendBroadcast(mediaScanIntent)
    }


}