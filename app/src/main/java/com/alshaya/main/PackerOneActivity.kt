package com.alshaya.main

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.adapter.PhotoGalleryAdapter
import com.alshaya.controls.CustomDialog
import com.alshaya.pojo.FileTypeBean
import com.alshaya.utils.*
import kotlinx.android.synthetic.main.activity_paker_one.*
import kotlinx.android.synthetic.main.custom_dialog.*
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File


class PackerOneActivity : BaseActivity() {

    var imageArrayList = ArrayList<FileTypeBean>()
    private lateinit var adapter: PhotoGalleryAdapter
    var uploadPojo = FileTypeBean()
    var scanBIllNumber = ""
    var awbBillNumber = ""
    var lastId = ""
    var isCheck = true
    lateinit var textWatcher: TextWatcher
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paker_one)
        activity = this
        storeUserData = StoreUserData(activity)
        getLastScanned()
        ivBack.setOnClickListener {
            finish()
        }

        checkPermissions()

        edILSOrder.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (!Utils.isEmpty(edILSOrder)) {
                    checkIlsNumber()
                }
            }

        })
        scanILSOrder.setOnClickListener {
            edILSOrder.setText("")
            edILSOrder.isEnabled = true
            edILSOrder.requestFocus()
        }

        edAWBOrder.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (!Utils.isEmpty(edAWBOrder)) {
                    if (edILSOrder.text.toString() == edAWBOrder.text.toString()) {
                        edAWBOrder.isEnabled = false
                        edBoxType?.requestFocus()
                    } else {
                        showAlert("ILS Order Number and AWB ILS Order Number are not same!")
                        edAWBOrder.text = null
                        edAWBOrder.requestFocus()
                    }
                }
            }

        })
        scanAWBOrder.setOnClickListener {
            edAWBOrder.setText("")
            edAWBOrder.isEnabled = true
            edAWBOrder.requestFocus()
        }

        setWatcher(edBoxType, null, scanBoxType)

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                if (isCheck) {
                    edAWBBill.isEnabled = false
                    edILSOrder?.requestFocus()
                    isCheck = false
                    prefixAWDNumber()
                }
            }

        }
        edAWBBill.addTextChangedListener(textWatcher)

        scanAWBBill.setOnClickListener {
            edAWBBill.setText("")
            edAWBBill.isEnabled = true
            edAWBBill.requestFocus()
        }

        imageArrayList.add(uploadPojo)
        adapter = PhotoGalleryAdapter(activity, imageArrayList, onPostImageClick)
        rvImages.adapter = adapter


        btnSubmit.setOnClickListener {
            if (imageArrayList.size <= 1) {
                showAlert("Please select minimum 1 image")
            } else {
                if (edILSOrder.text.toString() == edAWBOrder.text.toString()) {
                    addPacker()
                } else {
                    showAlert("ILS Order Number and AWB ILS Order Number are not same!")
                }

            }
        }

        deleteLastPacker.setOnClickListener {
            if (lastId.isNotEmpty())
                deletePacker()
        }
        Handler(Looper.getMainLooper()).postDelayed({
            Utils.hideKB(activity, edAWBBill)
        }, 300)
    }

    private fun deletePacker() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().deletePacker(
                storeUserData.getString(Constants.USER_TOKEN),
                lastId
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                getLastScanned()
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun checkPermissions(): Boolean {
        val hasStoragePermission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
        )
        val hasCameraPermission = ContextCompat.checkSelfPermission(
            activity,
            Manifest.permission.CAMERA,
        )
        val permissions: MutableList<String> =
            ArrayList()
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA)
        }
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        return if (permissions.isNotEmpty()) {
            requestPermissions(
                permissions.toTypedArray(),
                103
            )
            false
        } else {
            true
        }
    }


    private fun getLastScanned() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().packer(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                val json = JSONObject(responseString).getJSONObject("last_data")

                if (json.length() != 0) {
                    lastId = json.getString("id")
                    if (!json.getString("awb_bill_number").isNullOrEmpty()) {
                        tvAWBBill.text = json.getString("awb_bill_number")
                    }
                    if (!json.getString("ils_order_no").isNullOrEmpty()) {
                        tvILSOrder.text = json.getString("ils_order_no")

                    }
                    if (!json.getString("box_type").isNullOrEmpty()) {
                        tvBoxType.text = json.getString("box_type")
                    }

                    deleteLastPacker.isVisible = json.getInt("allowed_delete") == 1

                } else {
                    lastId = ""
                    tvAWBBill.text = null
                    tvILSOrder.text = null
                    tvBoxType.text = null
                }

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                tvAWBBill.text = null
                tvILSOrder.text = null
                tvBoxType.text = null
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun checkIlsNumber() {
        Log.i("TAG", "token: " + storeUserData.getString(Constants.USER_TOKEN))
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().checkIlsNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                edAWBBill.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                edILSOrder.isEnabled = false
                edAWBOrder?.requestFocus()
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                edILSOrder.text = null
                edILSOrder.isEnabled = true
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun prefixAWDNumber() {
        Log.i("TAG", "token: " + storeUserData.getString(Constants.USER_TOKEN))
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().awbNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                edAWBBill.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var json = JSONObject(responseString)
                scanBIllNumber = json.getString("scan_awb_bill_number")
                awbBillNumber = json.getString("awb_bill_number")
                edAWBBill.setText(json.getString("awb_bill_number"))
                isCheck = true
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                edAWBBill.text = null
                edAWBBill.isEnabled = true
                edAWBBill.requestFocus()
                isCheck = true
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun addPacker() {

        var listOfUploads = ArrayList<MultipartBody.Part>()
        if (imageArrayList.size > 1) {
            listOfUploads.add(RetrofitHelper.prepareFilePart("image_1", imageArrayList[1].name))
        }
        if (imageArrayList.size > 2) {
            listOfUploads.add(RetrofitHelper.prepareFilePart("image_2", imageArrayList[2].name))
        }
        if (imageArrayList.size > 3) {
            listOfUploads.add(RetrofitHelper.prepareFilePart("image_3", imageArrayList[3].name))
        }

        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().addPacker(
                storeUserData.getString(Constants.USER_TOKEN),
                RetrofitHelper.createPartFromString(awbBillNumber),
                RetrofitHelper.createPartFromString(edILSOrder.text.toString()),
                RetrofitHelper.createPartFromString(edAWBOrder.text.toString()),
                RetrofitHelper.createPartFromString(edBoxType.text.toString()),
                RetrofitHelper.createPartFromString(scanBIllNumber),
                listOfUploads,
                RetrofitHelper.createPartFromString(storeUserData.getString(Constants.SITE_ID)),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccessAddaPacker:$responseString")
                var alert:CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert=null
                    startActivity(Intent(activity, PackerOneActivity::class.java))
                    finish()
                })
                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private var onPostImageClick = object : OnImageClick {
        override fun onImageClick(position: Int, fileTypeBean: FileTypeBean) {
            if (checkPermissions()) {
                val isDeviceSupportCamera: Boolean =
                    packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
                var photoFile: File
                var fileProvider: Uri
                if (isDeviceSupportCamera) {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    photoFile = File(
                        activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        System.currentTimeMillis().toString() + ".jpg"
                    )
                    photoFile.createNewFile()
                    photoFile = File(photoFile.absolutePath)
                    fileProvider = FileProvider.getUriForFile(
                        activity,
                        "com.alshaya.eCOMTracker.provider",
                        photoFile
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)
                    activityLauncher.launch(intent) { result ->
                        if (result.resultCode == Activity.RESULT_OK) {
                            val file = FilePathUtil.copyUriToExternalFilesDir(
                                activity,
                                fileProvider,
                                System.currentTimeMillis().toString() + ".jpg"
                            )
                            var image = FileTypeBean()
                            image.is_update = 1
                            image.type = "image"
                            image.name = file!!.absolutePath
                            var imagesSize = imageArrayList.size

                            if (imagesSize > 3) {
                                Toast.makeText(
                                    activity,
                                    "You cannot add more than 3 images",
                                    Toast.LENGTH_LONG
                                ).show()

                            } else {
                                imageArrayList.add(image)

                            }

                            adapter.notifyDataSetChanged()
                        }
                    }

                } else {
                    Toast.makeText(activity, "camera not supported", Toast.LENGTH_SHORT).show()
                }
            } else {
                AlertDialog.Builder(activity)
                    .setMessage("You need to allow access to some permissions.")
                    .setPositiveButton("Ok", object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            DialogInterface.OnClickListener { dialog, which ->
                                val intent = Intent(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", packageName, null)
                                )
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                                dialog.dismiss()
                            }
                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show()

            }
        }

        override fun onImageDeleteClick(position: Int, fileTypeBean: FileTypeBean, id: String) {
            val alert = CustomDialog(activity)
            alert.setCancelable(false)
            alert.show()
            alert.setMessage("Are you sure want to delete?")
            alert.setPositiveButton("Yes", View.OnClickListener {
                if (fileTypeBean.is_update == 1) {
                    imageArrayList.removeAt(position)
                } else {
                    // deletePostImage(id)
                    imageArrayList.removeAt(position)
                }
                adapter.notifyDataSetChanged()
                alert.dismiss()
            })
            alert.setNegativeButton("No", View.OnClickListener {
                alert.dismiss()
            })


        }

        override fun onDelete(postId: Int, position: Int) {

        }
    }

}