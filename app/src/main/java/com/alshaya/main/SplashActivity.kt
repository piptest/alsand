package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatDelegate
import com.alshaya.R
import com.alshaya.utils.StoreUserData

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        activity = this
        storeUserData = StoreUserData(activity)
        setContentView(R.layout.activity_splash)
        Handler(Looper.getMainLooper()).postDelayed({
            /*if (!storeUserData.getString(Constants.USER_TOKEN).isNullOrEmpty()) {
                startActivity(
                    Intent(
                        activity,
                        HomeActivity::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            } else {*/
            startActivity(Intent(activity, LoginActivity::class.java))
            finish()
            //}
        }, 1500)
    }
}