package com.alshaya.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import com.alshaya.R
import com.alshaya.controls.CustomDialog
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import kotlinx.android.synthetic.main.activity_paker_one.*
import kotlinx.android.synthetic.main.activity_sagregation.*
import kotlinx.android.synthetic.main.activity_sagregation.btnSubmit
import kotlinx.android.synthetic.main.activity_sagregation.ivBack
import kotlinx.android.synthetic.main.custom_dialog.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class SagregationActivity : BaseActivity() {
    var lastSagrigationId = ""
    var lastSagrigationAwbumber = ""
    var isCheck = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sagregation)
        activity = this
        storeUserData = StoreUserData(activity)

        ivBack.setOnClickListener {
            finish()
        }

        /*if () {
            btnSubmit.setBackgroundResource(R.drawable.btn_bg)
            btnSubmit.isEnabled = true
        } else {
            btnSubmit.setBackgroundResource(R.drawable.btb_bg_light)
            btnSubmit.isEnabled = true
        }*/
        scanAwbNumber.setOnClickListener {
            sagregationAWBNumber.text = null
            sagregationAWBNumber.isEnabled = true
            sagregationAWBNumber.requestFocus()
        }
        sagregationPalletId.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (Utils.isEmpty(sagregationPalletId)) {
                    return
                }
                if (sagregationPalletId.text.toString().length == 12) {
                    sagregationPalletId.isEnabled = false
                    sagregationAWBNumber?.isEnabled = true
                    sagregationAWBNumber?.requestFocus()
                    if (!Utils.isEmpty(sagregationWeight) && !Utils.isEmpty(sagregationAWBNumber) && !Utils.isEmpty(
                            sagregationPalletId
                        )
                    ) {
                        btnSubmit.setBackgroundResource(R.drawable.btn_bg)
                        btnSubmit.isEnabled = true
                    } else {
                        btnSubmit.setBackgroundResource(R.drawable.btb_bg_light)
                        btnSubmit.isEnabled = false
                    }
                } else {
                    showAlert("Invalid Pallet id. Please enter 12 Character Pallet id.")
                    sagregationPalletId.text = null
                }
            }

        })

        sagregationAWBNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!isCheck) {
                    return
                }
                if (!Utils.isEmpty(sagregationAWBNumber)) {
                    sagregationAWBNumber.isEnabled = false
                    isCheck = false
                    checkAWBNumber()
                }

                if (!Utils.isEmpty(sagregationWeight) && !Utils.isEmpty(sagregationAWBNumber) && !Utils.isEmpty(
                        sagregationPalletId
                    )
                ) {
                    btnSubmit.setBackgroundResource(R.drawable.btn_bg)
                    btnSubmit.isEnabled = true
                } else {
                    btnSubmit.setBackgroundResource(R.drawable.btb_bg_light)
                    btnSubmit.isEnabled = false
                }
            }

        })


        sagregationWeight.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!Utils.isEmpty(sagregationWeight) && !Utils.isEmpty(sagregationAWBNumber) && !Utils.isEmpty(
                        sagregationPalletId
                    )
                ) {
                    sagregationWeight.isEnabled =false
                    btnSubmit.setBackgroundResource(R.drawable.btn_bg)
                    btnSubmit.isEnabled = true
                } else {
                    btnSubmit.setBackgroundResource(R.drawable.btb_bg_light)
                    btnSubmit.isEnabled = false
                }
            }
        })

        scanPalletId.setOnClickListener {
            sagregationPalletId.setText("")
            sagregationPalletId.isEnabled = true
            sagregationPalletId.requestFocus()
        }

        scanAwbNumber.setOnClickListener {
            sagregationAWBNumber.setText("")
            sagregationAWBNumber.isEnabled = true
            sagregationAWBNumber.requestFocus()
        }

        scanWeight.setOnClickListener {
            sagregationWeight.setText("")
            sagregationWeight.isEnabled = true
            sagregationWeight.requestFocus()
        }

        btnBackToHome.setOnClickListener {
            finish()
        }

        btnSubmit.setOnClickListener {
            when {
                Utils.isEmpty(sagregationPalletId) -> {
                    showAlert("Pallet ID is required.")
                }
                Utils.isEmpty(sagregationAWBNumber) -> {
                    showAlert("AWB Bill Number is required.")
                }
                Utils.isEmpty(sagregationWeight) -> {
                    showAlert("Weight is required.")
                }
                else -> {
                    addSagregation()
                }
            }
        }


        cbLockPallet.setOnCheckedChangeListener { _, isChecked ->
            if (Utils.isEmpty(sagregationPalletId)) {
                cbLockPallet.isChecked = false
            } else {
                if (isChecked) {
                    lockPalletId()
                    sagregationAWBNumber.isEnabled = false
                    scanAwbNumber.isEnabled = false
                    sagregationWeight.isEnabled = false
                    scanWeight.isEnabled = false
                    btnSubmit.isEnabled = false
                } else {
                    lockPalletId()
                    sagregationAWBNumber.isEnabled = true
                    scanAwbNumber.isEnabled = true
                    sagregationWeight.isEnabled = true
                    scanWeight.isEnabled = true
                    btnSubmit.isEnabled = true
                }
            }
        }

        cbCheckPalletID.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                if (!Utils.isEmpty(sagregationPalletId)) {
                    sagregationPalletId.isEnabled = false
                    sagregationPalletId.setText(sagregationPalletId.text.toString())
                } else {
                    sagregationPalletId.isEnabled = true
                }
            } else {

            }
        }




        llDeleteSagrigation.setOnClickListener {
            deleteLastSagrigation()
        }

        Handler(Looper.getMainLooper()).postDelayed({
            Utils.hideKB(activity, sagregationPalletId)
        }, 300)

    }

    override fun onResume() {
        super.onResume()
        getLastSagregation()

    }

    private fun lockPalletId() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().lockPalletId(
                storeUserData.getString(Constants.USER_TOKEN),
                sagregationPalletId.text.toString(),
                storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
               // showAlert(JSONObject(responseString).getString("message"))


                var alert:CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                })

                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun checkAWBNumber() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().prefixAwbSegregation(
                storeUserData.getString(Constants.USER_TOKEN),
                sagregationAWBNumber.text.toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                var data = JSONObject(responseString)//.getJSONObject("data")
                sagregationAWBNumber.setText(data.getString("awb_bill_number"))
                isCheck = true
                sagregationWeight.isEnabled = true
                sagregationWeight?.requestFocus()
                /*if (data.getString("sagregation_status") == "pending") {
                    sagregationWeight.isEnabled = true
                    sagregationWeight?.requestFocus()
                } else if (data.getString("sagregation_status") == "completed") {
                    sagregationAWBNumber.text = null
                    sagregationAWBNumber.requestFocus()
                    sagregationAWBNumber.isEnabled = true
                    showAlert("This Segregation is Already Completed")
                }*/
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                sagregationAWBNumber.text = null
                sagregationAWBNumber.requestFocus()
                sagregationAWBNumber.isEnabled = true
                showAlert(JSONObject(error!!).getString("message"))
                isCheck = true
            }
        })
    }

    private fun addSagregation() {
        Log.i("TAG", "weight: " + sagregationWeight.text.toString())
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().addSagregation(
                storeUserData.getString(Constants.USER_TOKEN),
                sagregationAWBNumber.text.toString(),
                sagregationPalletId.text.toString(),
                sagregationWeight.text.toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                sagregationAWBNumber.text!!.clear()
                sagregationWeight.text!!.clear()
                if (!cbCheckPalletID.isChecked) {
                    sagregationPalletId.text!!.clear()
                    scanPalletId.performClick()
                } else {
                    scanAwbNumber.performClick()
                }
                var alert:CustomDialog? = CustomDialog(activity)
                alert?.setCancelable(false)
                alert?.show()
                alert?.setMessage(JSONObject(responseString).getString("message"))
                alert?.setPositiveButton("ok", View.OnClickListener {
                    alert?.dismiss()
                    alert = null
                })

                Handler(Looper.getMainLooper()).postDelayed({
                    alert?.btnPositive?.performClick()
                }, 1500)

                getLastSagregation()
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun getLastSagregation() {
        Log.i("TAG", "token: " + storeUserData.getString(Constants.USER_TOKEN))
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().sagregation(
                storeUserData.getString(Constants.USER_TOKEN),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "getLastSagregation:$responseString")
                val json = JSONObject(responseString).getJSONObject("last_data")
                if (json.length() != 0) {
                    if (!json.getString("awb_bill_number").isNullOrEmpty()) {
                        tvAWBSagregation.text = json.getString("awb_bill_number")
                    }
                    if (!json.getString("pallet_id").isNullOrEmpty()) {
                        palletSagregation.text = json.getString("pallet_id")

                    }
                    if (!json.getString("weight").isNullOrEmpty()) {
                        weightSagregation.text = json.getString("weight")

                    }
                    if (!json.getInt("id").toString().isNullOrEmpty()) {
                        lastSagrigationId = json.getInt("id").toString()

                    }
                    if (!json.getString("awb_bill_number").isNullOrEmpty()) {
                        lastSagrigationAwbumber = json.getString("awb_bill_number")

                    }

                    llDeleteSagrigation.isVisible = json.getInt("allowed_delete") == 1
                }

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    private fun deleteLastSagrigation() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().deleteLastSagregation(
                storeUserData.getString(Constants.USER_TOKEN),
                lastSagrigationId,
                lastSagrigationAwbumber, storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccessDeleteLastSag:$responseString")

                showAlertDelete(JSONObject(responseString).getString("message"))

            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"))
            }
        })
    }

    fun showAlertDelete(message: String) {
        var alert:CustomDialog? = CustomDialog(activity)
        alert?.setCancelable(false)
        alert?.show()
        alert?.setTitle(title)
        alert?.setMessage(message)
        alert?.setPositiveButton("ok") {
            alert?.dismiss()
            alert = null
            onResume()
        }

        Handler(Looper.getMainLooper()).postDelayed({
            alert?.btnPositive?.performClick()
        }, 1500)
    }

}