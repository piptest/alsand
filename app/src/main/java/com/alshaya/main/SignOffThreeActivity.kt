package com.alshaya.main

import android.os.Bundle
import android.util.Log
import com.alshaya.R
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import kotlinx.android.synthetic.main.activity_sign_off_three.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class SignOffThreeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_off_three)
        activity = this
        storeUserData = StoreUserData(activity)


        trackAWBNumber()

        ivBack.setOnClickListener {
            finish()
        }

        llTrackAWBNumberList.setOnClickListener {
            //  startActivity(Intent(activity, PendingRequestActivity::class.java))
        }


    }

    private fun trackAWBNumber() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().trackAWBNumber(
                storeUserData.getString(Constants.USER_TOKEN),
                intent.getStringExtra("awb_bill_number").toString(), storeUserData.getString(Constants.SITE_ID),
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")

                tvTrackName.text = JSONObject(responseString).getString("status")
                tvTrackDate.text = JSONObject(responseString).getString("created_at")


            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                showAlert(JSONObject(error!!).getString("message"),true)
            }
        })
    }
}