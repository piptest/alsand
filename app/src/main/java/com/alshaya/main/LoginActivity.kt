package com.alshaya.main

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import com.alshaya.R
import com.alshaya.pojo.Login
import com.alshaya.pojo.Sites
import com.alshaya.utils.Constants
import com.alshaya.utils.RetrofitHelper
import com.alshaya.utils.StoreUserData
import com.alshaya.utils.Utils
import com.alshaya.utils.Utils.isEmpty
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response

class LoginActivity : BaseActivity() {
    var sites = ArrayList<Sites>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        activity = this
        storeUserData = StoreUserData(activity)
        edSite.setOnClickListener {
            val lists = ArrayList<String>()
            for (i in 0 until sites.size) {
                lists.add(sites[i].name)
            }
            val builder = AlertDialog.Builder(activity)
            val dataAdapter = ArrayAdapter(
                activity,
                android.R.layout.simple_dropdown_item_1line, lists
            )
            //pass custom layout with single textview to customize list
            builder.setAdapter(dataAdapter) { dialog, which ->
                edSite.text = sites[which].name
                storeUserData.setString(Constants.SITE_ID, sites[which].id)
            }
            val dialog = builder.create()
            dialog.show()
        }
        tvScanLogin.setOnClickListener {
            etLoginId.isEnabled = true
            etLoginId.text = null
            etLoginId.requestFocus()
        }
        etLoginId.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                if (!Utils.isEmpty(etLoginId)) {
                    etLoginId.isEnabled = false
                    login()
                }
            }

        })

        btnLogin.setOnClickListener {
            when {
                isEmpty(etLoginId) -> {
                    showAlert("Please scan employee ID.")
                }
                storeUserData.getString(Constants.USER_TOKEN).isNullOrEmpty() -> {
                    showAlert("Please scan proper employee ID.")
                }
                isEmpty(edCountry) -> {
                    showAlert("Please select country.")
                }
                isEmpty(edSite) -> {
                    showAlert("Please select site.")
                }
                else -> {
                    startActivity(
                        Intent(
                            activity,
                            HomeActivity::class.java
                        ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                }
            }
        }
    }

    private fun login() {
        showProgress()
        val retrofitHelper = RetrofitHelper()
        val call: Call<ResponseBody> =
            retrofitHelper.api().login(
                etLoginId.text.toString(),
                "Android",
                "1234"
            )

        retrofitHelper.callApi(activity, call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody>) {
                dismissProgress()
                val responseString = body.body()!!.string()
                Log.i("TAG", "onSuccess:$responseString")
                val login = Gson().fromJson(responseString, Login::class.java).data
                storeUserData.setString(
                    Constants.USER_TOKEN,
                    "Bearer " + JSONObject(responseString).getString("token")
                )
               storeUserData.setString(
                    Constants.USER_ID,
                    JSONObject(responseString).getString("id")
                )
                storeUserData.setString(
                    Constants.LOGIN_ID,
                    etLoginId.text.toString()
                )
                llAfterScan.visibility = View.VISIBLE
                edCountry.text = login.country_name
                sites.addAll(login.sites)
            }

            override fun onError(code: Int, error: String?) {
                dismissProgress()
                Log.i("TAG", "onError:$error")
                etLoginId.text = null
                etLoginId.isEnabled = true
                etLoginId.requestFocus()
                showAlert(JSONObject(error ?: "").optString("message"))
            }
        })
    }
}