package com.alshaya.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.alshaya.R
import com.alshaya.main.*
import com.alshaya.pojo.Home
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_home.view.*

class HomeAdapter(
    var activity: FragmentActivity,
    private var data: ArrayList<Home>,
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.row_home,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val pojo = data[position]
        Glide.with(activity).load(pojo.image).into(holder.itemView.ivHome)
        holder.itemView.tvHomeName.text = pojo.name
        holder.itemView.setOnClickListener {
            when (pojo.name) {
                "Packing" -> {
                    activity.startActivity(Intent(activity, PackerOneActivity::class.java))
                }
                "Segregation" -> {
                    activity.startActivity(Intent(activity, SagregationActivity::class.java))
                }
                "Dispatch" -> {
                    activity.startActivity(Intent(activity, DispatchActivity::class.java))
                }
                "STS" -> {
                    activity.startActivity(Intent(activity, StsActivity::class.java))
                }
                "Track / Pending Request" -> {
                    activity.startActivity(Intent(activity, TrackActivity::class.java))
                }
                "QC Checking" -> {
                    activity.startActivity(Intent(activity, QRCheckingActivity::class.java))
                }
            }


        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView)
}