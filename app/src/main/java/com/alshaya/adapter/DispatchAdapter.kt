package com.alshaya.adapter

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alshaya.R
import com.alshaya.pojo.ScanPalletIdPojo
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_awb_number.view.*

class DispatchAdapter(
    var activity: Activity,
    var items: ArrayList<ScanPalletIdPojo.BoxDataBean>

) : RecyclerView.Adapter<DispatchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_awb_number, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        holder.itemView.tvBillNumber.text = data.awb_bill_number
        if (data.isScanned == 1) {
            Glide.with(activity).load(R.drawable.ic_success).into(holder.itemView.ivApprove)
            holder.itemView.tvBillNumber.setTextColor(Color.parseColor("#1EBE89"))

        } else {
            Glide.with(activity).load(R.drawable.ic_close).into(holder.itemView.ivApprove)
            holder.itemView.tvBillNumber.setTextColor(Color.parseColor("#FF6F50"))
        }

    }

    inner class ViewHolder(view: View?) : RecyclerView.ViewHolder(view!!)

    override fun getItemCount(): Int {
        return items.size
    }


}