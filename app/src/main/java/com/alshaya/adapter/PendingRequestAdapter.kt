package com.alshaya.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.alshaya.R
import com.alshaya.pojo.TrackPalletMDRPojo
import kotlinx.android.synthetic.main.row_pending_request.view.*

class PendingRequestAdapter(
    var activity: Activity,
    var items: ArrayList<TrackPalletMDRPojo.PalletDetailsBean>

) : RecyclerView.Adapter<PendingRequestAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pending_request, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        holder.itemView.tvPalletId.text =
            "${if (data.type == "pallet") "Pallet Id:" else "MDR Id:"} " + data.pallet_id
        holder.itemView.tvStatus.isVisible = !data.message.isNullOrEmpty()
        holder.itemView.tvStatus.text = data.message
        /*holder.itemView.tvStatus.text =

            if (data.dispatch_status == "pending") {
                "Pending"
            } else if (data.sign_as_dispatcher_status == "pending") {
                "Pending Dispatcher Signature"
            } else if (data.sign_as_receiver_status == "pending") {
                "Pending Receiver Signature"
            } else {
                ""
            }
        holder.itemView.tvStatus.setTextColor(
            ContextCompat.getColor(
                activity, if (data.sign_as_receiver_status == "pending") {
                    R.color.orange
                } else if (data.dispatch_status == "pending") {
                    R.color.red
                } else {
                    R.color.black
                }
            )
        )*/

        /*holder.itemView.llMain.setOnClickListener {
            if (data.dispatch_status == "pending" && data.type == "pallet") {
                activity.startActivity(Intent(activity, STSReceivingActivity::class.java))
            } else if (data.sign_as_dispatcher_status == "pending" && data.type == "pallet") {
                activity.startActivity(
                    Intent(
                        activity,
                        SignOffActivity::class.java
                    ).putExtra("palletId", data.pallet_id)
                )
            } else if (data.sign_as_receiver_status == "pending" && data.type == "pallet") {
                activity.startActivity(
                    Intent(
                        activity,
                        SignOffActivity::class.java
                    ).putExtra("palletId", data.pallet_id)
                )
            } else if (data.sts_dispatch_status == "pending" && data.type == "mdr") {
                activity.startActivity(Intent(activity, STSDispatchActivity::class.java))
            } else if (data.sign_as_dispatcher_status == "pending" && data.type == "mdr") {
                activity.startActivity(
                    Intent(
                        activity,
                        STSDispatchSignOffActivity::class.java
                    ).putExtra("PalletID", data.pallet_id)
                )
            } else if (data.sign_as_receiver_status == "pending" && data.type == "mdr") {
                activity.startActivity(
                    Intent(
                        activity,
                        STSDispatchSignOffActivity::class.java
                    ).putExtra("PalletID", data.pallet_id)
                )
            } else {
                activity.startActivity(
                    Intent(activity, PendingRequestActivity::class.java).putExtra(
                        "MDRId",
                        data.pallet_id
                    )
                )
            }
        }*/

    }

    inner class ViewHolder(view: View?) : RecyclerView.ViewHolder(view!!)

    override fun getItemCount(): Int {
        return items.size
    }


}