package com.alshaya.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.alshaya.R
import com.alshaya.pojo.FileTypeBean
import com.alshaya.utils.Constants
import com.alshaya.utils.OnImageClick
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_supportimage.view.*
import java.io.File

class PhotoGalleryAdapter(
    var activity: Activity,
    private val arrayList: ArrayList<FileTypeBean>,
    private var onImageClick: OnImageClick

) : RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_supportimage, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pojo = arrayList[position]
        if (position == 0) {
            holder.itemView.llImage.isVisible = false
            holder.itemView.llUpload.isVisible = true
        } else {
            holder.itemView.llUpload.isVisible = false
            holder.itemView.llImage.isVisible = true
        }

        if (pojo.is_update == 1) {
            Glide.with(activity).load(File(pojo.name)).into(holder.itemView.image)
        } else {
            Glide.with(activity).load(Constants.URL + pojo.name).into(holder.itemView.image)
        }

        holder.itemView.tvPhoto.text = "Upload Photo"
        holder.itemView.ivDelete.setOnClickListener {
            onImageClick.onImageDeleteClick(position, pojo, pojo.id.toString())
        }
        holder.itemView.llUpload.setOnClickListener {
            var image = 0
            var video = 0
            for (i in arrayList) {
                if (i.type.equals("image", true)) {
                    image++
                }
            }
            onImageClick.onImageClick(position, pojo)

        }

    }

    inner class ViewHolder(view: View?) : RecyclerView.ViewHolder(view!!)

    override fun getItemCount(): Int {
        return arrayList.size
    }
}