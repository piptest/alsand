package com.alshaya.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alshaya.R
import kotlinx.android.synthetic.main.row_awb_number.view.*

class STSDispatchAdapter(
    var activity: Activity,
    var items: ArrayList<String>,


    ) : RecyclerView.Adapter<STSDispatchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_sts_dispatch, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = items[position]
        holder.itemView.tvBillNumber.text = data

        /*    holder.itemView.tvBillNumber.text = data.awb_bill_number
            Log.i("TAG", "awbBillNumber: $s")
            if (data.isScanned == 1) {
                Glide.with(activity).load(R.drawable.ic_success).into(holder.itemView.ivApprove)
                holder.itemView.tvBillNumber.setTextColor(Color.parseColor("#1EBE89"))

            } else {
                Glide.with(activity).load(R.drawable.ic_close).into(holder.itemView.ivApprove)
                holder.itemView.tvBillNumber.setTextColor(Color.parseColor("#FF6F50"))
            }*/

    }

    inner class ViewHolder(view: View?) : RecyclerView.ViewHolder(view!!)

    override fun getItemCount(): Int {
        return items.size
    }


}