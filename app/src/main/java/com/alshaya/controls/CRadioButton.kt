package com.alshaya.controls

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.AppCompatRadioButton

class CRadioButton : AppCompatRadioButton {

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        applyCustomFont(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        applyCustomFont(context, attrs)

    }

    private fun applyCustomFont(context: Context, attrs: AttributeSet) {
        val textStyle =
            attrs.getAttributeIntValue(CButton.ANDROID_SCHEMA, "textStyle", Typeface.NORMAL)
        typeface = selectTypeface(context, textStyle)
        isAllCaps = false
        if (gravity == Gravity.CENTER || gravity == Gravity.CENTER_HORIZONTAL || gravity == Gravity.CENTER_VERTICAL) {
        } else {
            textAlignment = View.TEXT_ALIGNMENT_VIEW_START
            textDirection = View.TEXT_DIRECTION_LOCALE
        }
    }

    private fun selectTypeface(context: Context, textStyle: Int): Typeface {
        when (textStyle) {
            Typeface.BOLD -> return Typeface.createFromAsset(context.assets, "Poppins-Medium.otf")
            Typeface.NORMAL -> return Typeface.createFromAsset(
                context.assets,
                "Poppins-Regular.otf"
            )// regular
            else -> return Typeface.createFromAsset(
                context.assets,
                "Poppins-Regular.otf"
            )// regular
        }
    }

}