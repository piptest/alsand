package com.alshaya.controls

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import com.alshaya.R
import kotlinx.android.synthetic.main.custom_progress_dialog.*

class CustomProgressDialog(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.custom_progress_dialog)
    }

    fun setTitle(message: String?) {
        tvProgressTitle.visibility = View.VISIBLE
        tvProgressTitle.text = message
    }

    override fun setTitle(string: Int) {
        tvProgressTitle.visibility = View.VISIBLE
        tvProgressTitle.setText(string)
    }
}